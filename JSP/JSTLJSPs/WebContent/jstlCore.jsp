<%--importar la libreria de JSTL CORE --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSTL Core</title>
</head>
<body>
	<h1>JTSL CORE, JSP STANDARD TAG LIBRARY</h1>
	<!-- Manipulacion de variables -->
	<!-- Definir variable -->
	<c:set var="nombre" value="Carlos"/>
	<!-- Desplegar valor de la variable -->
	Variable nombre: <c:out value="${nombre}"/>
	<br><br>
	Variable con HMTL
	<c:out value="<h4>Hola</h4>" escapeXml="false"/>
	<br><br>
	<!-- Codigo condicionado -->
	<c:set var="bandera" value="true"/>
	<c:if test="${bandera}">
		La bandera es verdadera
	</c:if> 
	<br><br>
	<!-- Switch -->
	<c:if test="${param.opcion != null }">
		<c:choose>
			<c:when test="${param.opcion == 1 }">Opcion 1 seleccionada</c:when>
			<c:when test="${param.opcion == 2 }">Opcion 2 seleccionada</c:when>
			<c:otherwise>Opcion desconocida ${param.opcion} </c:otherwise>
		</c:choose>
	</c:if>
	
	<!--  Iteracion de un arreglo -->
	<% 
		String [] nombres = {"claudio", "paloma", "nayelli"};
		request.setAttribute("nombres", nombres);
	%>
	<br><br>
	<ul>
		<c:forEach var="persona" items="${nombres}">
			<li>Nombre: ${persona}</li>
		</c:forEach>
	</ul>
	
	
	
</body>
</html>