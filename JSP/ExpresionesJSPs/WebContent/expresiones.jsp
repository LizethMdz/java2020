<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Expresiones</title>
</head>
<body>
	<h1>Expresiones JSP's</h1>
	<p>Concatenacion: <%= "Saludos"  + " " + "JSP" %> </p>
	<br>
	<p>Operacion matematica: <%= 2*9 / 5 %> </p>
	<br>
	<p>Session id: <%= session.getId() %></p>
	<br>
	<a href="index.html">Regresar</a>
</body>
</html>