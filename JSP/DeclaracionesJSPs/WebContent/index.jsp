<%-- Declaraciones --%>
<%!
	//Declaramos una variable cpn metodo get
	private String usuario = "juan";
	
	public String getUsuario(){
		return this.usuario;
	}
	
	//Declaramos el contador de visitas
	private int contadorVisitas = 1;
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Uso de declaraciones</title>
</head>
<body>
	<h1>Uso de declaraciones</h1>
	VALOR DE USUARIO: <%= this.usuario %>
	<br>
	VALOR DEL METODO <%= this.getUsuario() %>
	<br>
	CONTADOR DE VISITAS <%= this.contadorVisitas++ %>
</body>
</html>