<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GETTER BEAN</title>
</head>
<body>
	<h1>OBTENER UN JAVA BEAN</h1>
	<jsp:useBean id="rectangulo" class="beans.Rectangulo" scope="session"></jsp:useBean>
	Valor base: <jsp:getProperty property="base" name="rectangulo"/>
	<br/>
	Valor altura: <jsp:getProperty property="altura" name="rectangulo"/>
	<br/>
	Valor del area <jsp:getProperty name="rectangulo" property="area"></jsp:getProperty>
	
	<br/>
	
	<a href="index.jsp">Regresar</a>
</body>
</html>