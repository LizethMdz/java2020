<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SETTER BEAN</title>
</head>
<body>
	<h1>MODIFICAR UN JAVA BEAN</h1>
	<jsp:useBean id="rectangulo" class="beans.Rectangulo" scope="session"></jsp:useBean>
	
	Modificamos los atributos
	<br></br>
	<%
		int altura = 10;
		int base = 76;
	%>
	
	<jsp:setProperty property="base" name="rectangulo" value="<%= base %>"/>
	
	<jsp:setProperty property="altura" name="rectangulo" value="<%= altura %>"/>
	
	Nuevo valor de base: <%= base %>
	
	Nuevo valor de altura: <%= altura %>
	
	<br/>
	<a href="index.jsp">Regresar</a>
	
</body>
</html>