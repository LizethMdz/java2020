<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inclusion Estatica</title>
</head>
<body>
	<h1>Inclusion Estatica </h1>
	<br>
	<ul>
		<li><%@include file="paginas/noticias1.html" %></li>
		<li><%@include file="paginas/noticias2.html" %></li>
	</ul>
</body>
</html>