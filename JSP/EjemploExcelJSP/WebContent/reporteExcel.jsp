<%-- CREACION DE UN JSP DE RROR --%>
<%@page errorPage="/WEB-INF/errorException.jsp" %>
<%-- IMPORTACION DE LIBRERIAS Y CLASES --%>
<%@page import="utilerias.Conversiones, java.util.Date" %>
<%-- Tipo de contenido --%>
<%@page contentType="application/vnd.ms-excel" %>
<%-- Scriptlet --%>
<%
	String nombreArchivo = "reporte.xls";
	response.setHeader("Content-Disposition", "inline;filename="+ nombreArchivo);
%>
<%@ page language="java"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP reporteExcel</title>
</head>
<body>
	<h1>Excel!!</h1>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">Curso</th>
	      <th scope="col">Descripcion</th>
	      <th scope="col">Fecha</th>
	     
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	     
	      <td>1. Fundamentos</td>
	      <td>Sintaxis basica</td>
	      <td><%= Conversiones.format(new Date()) %></td>
	    </tr>
	    <tr>
	     
	      <td>2. Programacion con java</td>
	      <td>Programacion con POO</td>
	      <td><%= Conversiones.format(new Date("500")) %></td>
	    </tr>
	    <tr>
	   
	      <td>3. Ejercicios</td>
	      <td>Lo visto en clase</td>
	      <td><%= Conversiones.format(new Date()) %></td>
	    </tr>
	  </tbody>
	</table>
	
</body>
</html>