<%-- LIBRERIAS --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP page</title>
</head>
<body>
	<h1>Hello World!!</h1>
	<ul>
		<li><% out.println("Hola Mundo con Scriptlet"); %></li>
		<li>${"Hola mundo con Expression Language (EL)"}</li>
		<li><%= "Hola mundo con Expresiones" %></li>
		<li><c:out value="HolaMundo con JSTL" /></li>
	</ul>
</body>
</html>