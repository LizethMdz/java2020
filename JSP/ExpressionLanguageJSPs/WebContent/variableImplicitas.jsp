<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Variable implicitas</title>
</head>
<body>
	<h1>Variable implicitas</h1>
    <ul>
      <li>Nombre de la aplicaion ${pageContext.request.contextPath}</li>
      <li>Nombre del navegador ${header["User-Agent"]}</li>
      <li>ID de la sesion ${cookie.JSESSIONID.value}</li>
      <li>Web server: ${pageContext.servletContext.serverInfo}</li>
      <li>Valor parametro: ${param["usuario"]}</li>
    </ul>
	<br>
</body>
</html>
