<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Scriplet</title>
</head>
<body>
	<h1>JSP con Scriptlet</h1>
	<br>
	<%-- No se despliega - Enviar informacion al navegador --%>
	<%
		out.print("Salidos desde un scriptlet");
	%>
	<%-- Manupular objetos implicitos --%>
	<%
		String nombreApp = request.getContextPath();
		out.print("Nombre de la App: " + nombreApp);
	%>
	<br>
	<%-- Codigo condicionado --%>
	<%
		if(session != null && session.isNew()){
	%>
			la sesion es nueva
	<%-- Imprimir la sesion nueva --%>
	<%
		}else if(session != null) {
	%>
			la sesion no es nueva
	<% } %>
	<br>
	<a href="index.html">Regresar</a>

</body>
</html>