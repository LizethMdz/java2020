<%
	String fondo = request.getParameter("color");
	if(fondo == null || fondo.trim().equals("")){
		fondo="white";
	}
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Cambio de color</title>
</head>
<body bgcolor="<%= fondo %>">
	Color de fondo de aplicado: <%= fondo %>
	<br>
	<a href="index.html">Regresar</a>
</body>
</html>