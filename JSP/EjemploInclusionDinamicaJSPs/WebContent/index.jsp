<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inclusion Dinamica</title>
</head>
<body>
	<h1>Inclusion Dinamica </h1>
	<br>
	<ul>
		<li><jsp:include page="paginas/recursoPublico.jsp"/></li>
		<li>
			<jsp:include page="WEB-INF/recursoPrivado.jsp">
			<jsp:param name="colorFondo" value="yellow"/>
			</jsp:include>
		</li>
	</ul>
</body>
</html>