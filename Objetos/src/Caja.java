public class Caja {
    int ancho;
    int altura;
    int profundo;

    public Caja (){
    }

    public Caja (int ancho, int altura, int profundo){
        this.ancho = ancho;
        this.altura = altura;
        this.profundo = profundo;
    }

    public Integer volumen(){
        int volumen = this.ancho * this.altura * this.profundo;
        return volumen;
    }


}