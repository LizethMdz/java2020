package com;

import java.util.Date;

import com.herencia.Cliente;
import com.herencia.Empleado;

public class App {
    
    public static void main (String[] args){
        Empleado employee = new Empleado("Juan", 23123);
        employee.setEdad(12);
        employee.setApellido("Cardenas");
        employee.setGenero("M");
        employee.setDomicilio("Candiles #32");

        System.out.println(employee);

        Cliente client = new Cliente(new Date(), true);
        client.setNombre("Katia");
        client.setEdad(43);
        client.setApellido("Salinas");
        client.setGenero("F");
        client.setDomicilio("Nicaragua #543");
        System.out.println(client);
    }
}