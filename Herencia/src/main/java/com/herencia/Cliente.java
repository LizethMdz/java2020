package com.herencia;

import java.util.Date;

public class Cliente extends Persona {
    private int idCliente;
    private Date fechaRegistro;
    private boolean vip;
    private static int contadorClientes;

    public Cliente(Date fecha, boolean vip){
        this.fechaRegistro = fecha;
        this.vip = vip;
        this.idCliente = ++ contadorClientes;
    }

    public int getIdCliente(){
        return this.idCliente;
    }

    public Date getFechaRegistro(){
        return fechaRegistro;
    }

    public boolean vip(){
        return this.vip;
    }

    public void setIdCliente(int idCliente){
        this.idCliente = idCliente;
    }

    public void setFechaRegistro(Date fecha){
        this.fechaRegistro = fecha;
    }

    public void setVip(Boolean vip){
        this.vip = vip;
    }

    public static int getContadorClientes(){
        return contadorClientes;
    }

    @Override
    public String toString(){
        return  super.toString() + " ID Cliente: " + idCliente + " VIP: " + vip + " Fecha Registro: "  + fechaRegistro;
    }



}