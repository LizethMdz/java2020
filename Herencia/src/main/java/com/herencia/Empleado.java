package com.herencia;

public class Empleado extends Persona {
    private Integer id_empleado;
    private double sueldo;
    private static int contadorEmpleado;

    public Empleado (String nombre, double sueldo){
        super(nombre);
        this.id_empleado = ++contadorEmpleado;
        this.sueldo = sueldo;
    }

    public Integer getIdEmpleado(){
        return this.id_empleado;
    }

    public double getSueldo(){
        return this.sueldo;
    }

    public void setSueldo(double sueldo){
        this.sueldo = sueldo;
    }

    public static int getContadorEpleado(){
        return contadorEmpleado;
    }

    @Override
    public String toString(){
        return  super.toString() + " ID: " + id_empleado + " Sueldo: " + sueldo;
    }



}