package com.herencia;

public class Persona {
    
    private String nombre;
    private String apellido;
    private Integer edad;
    private String genero;
    private String domicilio;

    public Persona (){}

    public Persona (String nombre){
        this.nombre = nombre;
    }

    public Persona (String nombre, String apellido, Integer edad, String genero, String domicilio){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
        this.domicilio = domicilio;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public Integer getEdad() {
        return this.edad;
    }

    public String getGenero() {
        return this.genero;
    }

    public String getDomicilio() {
        return this.domicilio;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public void setEdad(Integer edad){
        this.edad = edad;
    }

    public void setGenero(String genero){
        this.genero = genero;
    }

    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }

    public String toString(){
        return " Nombre: " + nombre + " Apellido: " + apellido +  " Edad " + edad + " Genero: " + genero + " Domicilio: " + domicilio;
    }


}