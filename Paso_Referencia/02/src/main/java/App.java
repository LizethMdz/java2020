public class App {
    
    public static void main(String[] args) throws Exception {

        Persona persona = new Persona();
        persona.cambiarNombre("Juan");

        System.out.println("Valor por referencia: " + persona.obtenerNombre());
        
        modificarPersona(persona);

        System.out.println("Valor por referencia pero modificado: " + persona.obtenerNombre());
    }

	private static void modificarPersona(Persona persona) {
		// TODO Auto-generated method stub
        persona.cambiarNombre("Kevin");
	}
}