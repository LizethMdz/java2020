package com.jorgesa.proyecto.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jorgesa.proyecto.dtos.AlumnoDto;

@RestController
public class AlumnoController {

	@RequestMapping(path = "/alumno/obtener/saludo", method = RequestMethod.POST)
	public ResponseEntity<?> saludar(@RequestBody AlumnoDto alumno){
		String resultado = alumno.getNombre() + " te vas a recurse.";
		ResponseEntity<?> response = new ResponseEntity<>(resultado, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(path = "/alumno/obtener/saludo", method = RequestMethod.GET)
	public ResponseEntity<?> saludar(){
		ResponseEntity<?> response = new ResponseEntity<>("ok", HttpStatus.OK);
		return response;
	}
	
}