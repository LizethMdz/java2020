package com.jorgesa.proyecto.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class AlumnoDto {

	private String nombre;
	
	private String appPaterno;
	
	private String appMaterno;
	
}
