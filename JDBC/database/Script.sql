CREATE DATABASE  IF NOT EXISTS `jdbc`;
USE `jdbc`;

CREATE TABLE persona (
	id_persona INT NOT NULL,
	nombre VARCHAR(45),
	apellido VARCHAR(45),
	email VARCHAR(45),
	telefono VARCHAR(45),
	PRIMARY KEY (id_persona)
);

