package mx.com.jdbc.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.com.jdbc.domain.Usuario;

public class UsuarioJDBC {
	private static final String SQL_SELECT = "SELECT id_usuario, username, password FROM usuario";
	private static final String SQL_INSERT = "INSERT INTO usuario(username, password) VALUES(?,?)";
	private static final String SQL_UPDATE = "UPDATE usuario SET username=?, password=? WHERE id_usuario=?" ;
	private static final String SQL_DELETE = "DELETE FROM usuario WHERE id_usuario=?";
	
	public List<Usuario> select(){
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Usuario usuario = null;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_SELECT);
			rs = st.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("id_usuario");
				String username = rs.getString("username");
				String password = rs.getString("password");
			
				usuario = new Usuario();
				usuario.setId_usuario(id);
				usuario.setUsername(username);
				usuario.setPassword(password);

				
				usuarios.add(usuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			Conexion.close(rs);
			Conexion.close(st);
			Conexion.close(conn);
		}
		return usuarios;
	}

	public int insert(Usuario usuario){
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;

		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_INSERT);
			st.setString(1, usuario.getUsername());
			st.setString(2, usuario.getPassword());

			System.out.println("Ejecutando query: " + SQL_INSERT);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		return rows;
	}
	
	
	public int update(Usuario usuario) {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_UPDATE);
			st.setString(1,  usuario.getUsername());
			st.setString(2,  usuario.getPassword());
			st.setInt(3,  usuario.getId_usuario());
			
			System.out.println("Ejecutando query: " + SQL_UPDATE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		
		return rows;
		
	}
	
	public int delete(Usuario usuario) {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_DELETE);
			st.setInt(1, usuario.getId_usuario());
			System.out.println("Ejecutando query: " + SQL_DELETE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		
		return rows;
		
	}
}
