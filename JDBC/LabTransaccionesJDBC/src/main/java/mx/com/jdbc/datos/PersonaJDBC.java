package mx.com.jdbc.datos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import mx.com.jdbc.domain.Persona;

/*
 * Interacción con la base de datos
 * */
public class PersonaJDBC {

	private Connection conexionTransaccional;
	
	private static final String SQL_SELECT = "SELECT id_persona, nombre, apellido, email, telefono FROM persona";
	private static final String SQL_INSERT = "INSERT INTO persona(nombre, apellido, email, telefono) VALUES(?,?,?,?)";
	private static final String SQL_UPDATE = "UPDATE persona SET nombre=?, apellido=?, email=?, telefono=? WHERE id_persona=?" ;
	private static final String SQL_DELETE = "DELETE FROM persona WHERE id_persona=?";
	
	public PersonaJDBC(){}

	/*Recibe una conexion externa*/
	public PersonaJDBC(Connection conexionTransaccional){
		this.conexionTransaccional = conexionTransaccional;
	}

	/*Cambiamos el tipo de excepcion, ya que la propagamos*/
	public List<Persona> select() throws SQLException{
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Persona persona = null;
		List<Persona> personas = new ArrayList<Persona>();
		try {
			conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
			st = conn.prepareStatement(SQL_SELECT);
			rs = st.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("id_persona");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido");
				String email = rs.getString("email");
				String telefono = rs.getString("telefono");
				
				persona = new Persona();
				persona.setId_persona(id);
				persona.setNombre(nombre);
				persona.setApellido(apellido);
				persona.setEmail(email);
				persona.setTelefono(telefono);
				
				personas.add(persona);
			}
		}
		finally {
			Conexion.close(rs);
			Conexion.close(st);
			if( this.conexionTransaccional ==null){
				Conexion.close(conn);
			}
		}
		return personas;
	}
	
	
	public int insert(Persona persona) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
			st = conn.prepareStatement(SQL_INSERT);
			st.setString(1,  persona.getNombre());
			st.setString(2,  persona.getApellido());
			st.setString(3,  persona.getEmail());
			st.setString(4,  persona.getTelefono());
			
			System.out.println("Ejecutando query: " + SQL_INSERT);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} 
		finally {
			Conexion.close(st);
			if( this.conexionTransaccional ==null){
				Conexion.close(conn);
			}
		}
		return rows;
	}
	
	public int update(Persona persona) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
			st = conn.prepareStatement(SQL_UPDATE);
			st.setString(1,  persona.getNombre());
			st.setString(2,  persona.getApellido());
			st.setString(3,  persona.getEmail());
			st.setString(4,  persona.getTelefono());
			st.setInt(5, persona.getId_persona());
			
			System.out.println("Ejecutando query: " + SQL_UPDATE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		}
		
		finally {
			Conexion.close(st);
			if( this.conexionTransaccional ==null){
				Conexion.close(conn);
			}
		}
		
		return rows;
		
	}
	
	public int delete(Persona persona) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
			st = conn.prepareStatement(SQL_DELETE);
			st.setInt(1, persona.getId_persona());
			System.out.println("Ejecutando query: " + SQL_DELETE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		}
		
		finally {
			Conexion.close(st);
			if( this.conexionTransaccional ==null){
				Conexion.close(conn);
			}
		}
		
		return rows;
		
	}
}
