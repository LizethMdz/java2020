package mx.com.jdbc.LabTransaccionesJDBC;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import mx.com.jdbc.datos.Conexion;
import mx.com.jdbc.datos.PersonaJDBC;
import mx.com.jdbc.datos.UsuarioJDBC;
import mx.com.jdbc.domain.Persona;
import mx.com.jdbc.domain.Usuario;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	Connection conexion = null;
        try {
			conexion = Conexion.getConnection();
			if ( conexion.getAutoCommit()) {
				conexion.setAutoCommit(false);
			}
			
			PersonaJDBC persona = new PersonaJDBC(conexion);
			UsuarioJDBC usuario = new UsuarioJDBC(conexion);
			
			Persona maria = new Persona();
	        maria.setId_persona(2);
	        maria.setNombre("Margarita");
	        maria.setApellido("Salinas");
	        maria.setEmail("mg@gmail.com");
	        maria.setTelefono("4427651238");
			
			persona.update(maria);
			
			/*Persona juan = new Persona();
	        juan.setNombre("Juan");
	        juan.setApellido("Olveral");
	        juan.setEmail("jo@gmail.com");
	        juan.setTelefono("4427981247");*/
	        
	        //persona.insert(juan);
	        
	        Usuario karla = new Usuario();
	        karla.setUsername("karlanieves");
	        karla.setPassword("5465");
	        
	        usuario.insert(karla);
	        
	        conexion.commit();
	        System.out.println("Se hizo la transaccion");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			System.out.println("Entramos al rollback");
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(System.out);
			}
		}
        
        
        /**Persona juan = new Persona();
        juan.setNombre("Juan");
        juan.setApellido("Olvera");
        juan.setEmail("jo@gmail.com");
        juan.setTelefono("4427981247");
        persona.insert(juan);**/
        
        
        /**Persona maria = new Persona();
        maria.setId_persona(2);
        maria.setNombre("Maria");
        maria.setApellido("Guitierrez");
        maria.setEmail("mg@gmail.com");
        maria.setTelefono("4427651237");
        
        persona.update(maria);*/
        
        /**Persona juan2 = new Persona();
        juan2.setId_persona(3);
        persona.delete(juan2);**/
        
    }
}
