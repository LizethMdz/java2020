package mx.com.jdbc.datos;

import java.sql.*;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class Conexion {
	
	private static final String JDBC_URL = "jdbc:mysql://localhost:3306/jdbc?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private static final String JDBC_USER = "root";
	private static final String JDBC_PASSWORD = "admin";
	
	/*Pool de conexiones*/
	public static DataSource getDataSource() {
		BasicDataSource ds = new BasicDataSource();
		ds.setUrl(JDBC_URL);
		ds.setUsername(JDBC_USER);
		ds.setPassword(JDBC_PASSWORD);
		//Definimos el tamaño inicial sel pool
		ds.setInitialSize(5);
		return ds;
	}
	
	
	public static Connection getConnection() throws SQLException{
		//return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
		return getDataSource().getConnection();
	}
	
	public static void close(ResultSet rs) {
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
	}
	
	public static void close(PreparedStatement statement) {
		try {
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
	}
	
	public static void close(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
	}
	
	
	


}
