package mx.com.jdbc.datos;

import java.sql.SQLException;
import java.util.List;
import mx.com.jdbc.domain.UsuarioDto;

public interface UsuarioDao {
	
	public List<UsuarioDto> select() throws SQLException;
	
	public int insert(UsuarioDto persona) throws SQLException;
	
	public int update(UsuarioDto persona) throws SQLException;
	
	public int delete(UsuarioDto persona) throws SQLException;
}
