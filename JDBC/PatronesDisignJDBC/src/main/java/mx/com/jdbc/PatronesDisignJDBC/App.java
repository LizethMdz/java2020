package mx.com.jdbc.PatronesDisignJDBC;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import mx.com.jdbc.datos.Conexion;
import mx.com.jdbc.datos.PersonaDao;
import mx.com.jdbc.datos.UsuarioDao;
import mx.com.jdbc.datos.PersonaDaoImplJDBC;
import mx.com.jdbc.datos.UsuarioDaoImplJDBC;
import mx.com.jdbc.domain.PersonaDto;
import mx.com.jdbc.domain.UsuarioDto;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	Connection conexion = null;
        try {
			conexion = Conexion.getConnection();
			if ( conexion.getAutoCommit()) {
				conexion.setAutoCommit(false);
			}
			
			PersonaDao personaDao = new PersonaDaoImplJDBC(conexion);
			List<PersonaDto> personas = personaDao.select();
			
			for(PersonaDto persona: personas ) {
				System.out.println("Persona DTO: " + persona);
			}
			
			UsuarioDao usuarioDao = new UsuarioDaoImplJDBC(conexion);
			List<UsuarioDto> usuarios = usuarioDao.select();
			
			for(UsuarioDto usuario: usuarios ) {
				System.out.println("Usuario DTO: " + usuario);
			}
	        
	        conexion.commit();
	        System.out.println("Se hizo la transaccion");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			System.out.println("Entramos al rollback");
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace(System.out);
			}
		}
        
    }
}
