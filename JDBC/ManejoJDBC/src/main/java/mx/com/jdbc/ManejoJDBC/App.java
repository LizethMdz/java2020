package mx.com.jdbc.ManejoJDBC;

import java.util.List;

import mx.com.jdbc.datos.PersonaJDBC;
import mx.com.jdbc.domain.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        PersonaJDBC persona = new PersonaJDBC();
        List<Persona> personas = persona.select();
        
        for(Persona personakey: personas) {
        	System.out.println(personakey);
        }
        
        /**Persona juan = new Persona();
        juan.setNombre("Juan");
        juan.setApellido("Olvera");
        juan.setEmail("jo@gmail.com");
        juan.setTelefono("4427981247");
        persona.insert(juan);**/
        
        
        /**Persona maria = new Persona();
        maria.setId_persona(2);
        maria.setNombre("Maria");
        maria.setApellido("Guitierrez");
        maria.setEmail("mg@gmail.com");
        maria.setTelefono("4427651237");
        
        persona.update(maria);*/
        
        /**Persona juan2 = new Persona();
        juan2.setId_persona(3);
        persona.delete(juan2);**/
        
    }
}
