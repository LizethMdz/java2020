package mx.com.jdbc.IntroduccionJDBC;

import java.sql.*;

public class ConexionJDBC {
	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/jdbc?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		//Paso 2 Crear elobjeto a la base de datos
		try {
			Connection conexion = DriverManager.getConnection(url, "root", "admin");
			//Paso 3. Crear un objeto de tipo Statement
			Statement instruccion = conexion.createStatement();
			//Paso 4. Creamos query
			String query = "SELECT id_persona, nombre, apellido, email, telefono FROM persona";
			//Paso 5. Ejecución del query
			ResultSet resultado = instruccion.executeQuery(query);
			
			while(resultado.next()) {
				System.out.println("Persona: " + resultado.getInt(1));
				System.out.println("Nombre: " + resultado.getString(2));
				System.out.println("Apellido: " + resultado.getString(3));
				System.out.println("Email: " + resultado.getString(4));
				System.out.println("Telefono: " + resultado.getString(5));
			}
			
			resultado.close();
			instruccion.close();
			conexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
