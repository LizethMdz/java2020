package test;

public class Application {

    public static void main(String [] args){
        Suma sum = createObjetoSuma();

        System.out.println(sum.Sumar());
    }
    
    

	public static Suma createObjetoSuma() {
		return new Suma(4,8);
	}

}

class Suma{
    int a;
    int b;

    public Suma(int a, int b){
        this.a  = a;
        this.b = b;
    }

    public int Sumar(){
        return this.a + this.b;
    }
}