package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookiesServlet
 */
@WebServlet("/CookiesServlet")
public class CookiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookiesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean nuevoUsuario = true;
		//Obtener todas las cookies
		Cookie[] cookies = request.getCookies();
		
		//Validar si hay cookie creada con anterioridad
		if(cookies != null) {
			for(Cookie c: cookies) {
				if(c.getName().equals("visitanteRecurrente") && c.getValue().equals("si")) {
					//Si ya exite
					nuevoUsuario = false;
					break;
				}
			}
		}
		
		String mensaje = null;
		
		if(nuevoUsuario) {
			Cookie visitanteCookie = new Cookie("visitanteRecurrente", "si");
			response.addCookie(visitanteCookie);
			mensaje = "Gracias por visitar nuestro sitio por primera vez";
		}else {
			mensaje = "Gracias por visitar nuevamente nuestro sitio";
		}
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print("Mensaje: " + mensaje);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
