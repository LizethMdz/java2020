package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CarritoServlet
 */
@WebServlet("/CarritoServlet")
public class CarritoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarritoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8");
		
		//Crear  U Obtener el objeto HttpSession
		HttpSession sesion = request.getSession();
		//Recuperar los artiulos
		List<String> articulos = (List<String>) sesion.getAttribute("carrito");
		if(articulos == null) {
			//Inicializar la lista de articulos
			articulos = new ArrayList<>();
			sesion.setAttribute("carrito", articulos);
		}
		//Obtener el articulo
		String articuloN = request.getParameter("articulo");
		//Revisar y agregar el articulo
		if(articuloN != null && !articuloN.trim().equals("")) {
			articulos.add(articuloN);
		}
		
		PrintWriter out = response.getWriter();
		out.println("Lista de articulos");
		for(String articulo: articulos) {
			out.println("<li>");
			out.println("Articulo: " + articulo);
			out.println("</li>");
		}
		out.println("<br>");
		out.println("<a href='/CarritoComprasServlets'>Regresar</a>");
		out.close();
	}
	
	

}
