package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8"); 
		String usuario = request.getParameter("usuario");
		String password = request.getParameter("password");
		String tecn[] = request.getParameterValues("tec");
		String genero = request.getParameter("genero");
		String ocupacion = request.getParameter("ocupacion");
		String musica[] = request.getParameterValues("musica");
		String comentarios = request.getParameter("comentario"); 
		
		System.out.println(usuario);
		System.out.println(password);
		
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.print("<body>");
		out.print("El parametro usuario es: " + usuario);
		out.print("<br>");
		out.print("El parametro password es: " + password);
		out.print("<br/>");
		out.print("El parametro tecnologias es: ");
		for(String t: tecn) {
			out.print(t);
			out.print("/");
		}
		
		out.print("<br/>");
		out.print("El parametro genero es: " + genero);
		out.print("<br/>");
		out.print("El parametro ocupacion es: " + ocupacion);
		out.print("<br/>");
		out.print("El parametro musica es: " + musica);
		if(musica != null) {
			for(String m: musica) {
				out.print(m);
				out.print("/");
			}
		}else {
			out.print("No hay musica preferida");
		}
		
		out.print("<br/>");
		
		out.print("El parametro comentarios es: " + comentarios);
		out.print("<br/>");
		out.print("</body>");
		out.println("</html>");
		out.close();
	}

}
