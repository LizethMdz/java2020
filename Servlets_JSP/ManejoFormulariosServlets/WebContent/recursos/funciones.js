/**
 * Funciones de JavaScript
 */

function validarForma(forma){
	var usuario= forma.usuario;
	if(usuario.value =="" || usuario.value == "Escribir usuario"){
		alert("Debe proporcionar un nombre de usuario");
		usuario.focus();
		usuario.select();
		return false;
	}
	var password = forma.password;
	if(password.value == "" || password.length < 3){
		alert("Debe proporcionar un password mayor a 3 caracteres");
		password.focus();
		password.select();
		return false;
	}
	var check = forma.tec;
	var checkSeleccionado = false;
	
	for(var i=0; i < check.length; i++){
		if(check[i].checked){
			checkSeleccionado = true;
		}
	}
	
	if(!checkSeleccionado){
		alert("Debe seleccionar una de las tecnologias");
		return false;
	}
	
	var generos = forma.genero;
	var radioSeleccionado = false;
	for(var i=0; i< generos.length; i++){
		if(generos[i].checked){
			radioSeleccionado = true;
		}
	}
	
	if(!radioSeleccionado){
		alert("Debe seleccionar un genero");
		return false;s
	}
	
	var ocupaciones = forma.ocupacion;
	if(ocupaciones.value == ""){
		alert("Debe seleccionar una ocupacion");
		return false;
	}
	
	alert("Formulario valido");
	return true;
	
}