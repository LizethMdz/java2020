package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionServlet
 */
@WebServlet("/SessionServlet")
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8");
		//Verifica si existe o no una sesion, de lo contrario la crea
		HttpSession sesion = request.getSession();
		String titulo = null;
		//Pedir el atributo contador Visitas
		Integer contadorVisitas = (Integer) sesion.getAttribute("contadorVisitas");
		if(contadorVisitas == null) {
			contadorVisitas = 1;
			titulo = "Bienvenido por primera vez!!";
			
		}else {
			contadorVisitas++;
			titulo = "Bienvenido otra vez!!";
		}
		
		//Agregamos el nuevo valor a la sesion
		sesion.setAttribute("contadorVisitas", contadorVisitas);
		
		//Respuesta al cliente
		
		
		PrintWriter out = response.getWriter();
		out.println(titulo);
		out.println("No. accesos al recurso: " + contadorVisitas);
		out.println("<br>");
		out.println("ID de la sesion: " + sesion.getId());
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
