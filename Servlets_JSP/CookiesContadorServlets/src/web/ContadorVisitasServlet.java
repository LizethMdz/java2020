package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ContadorVisitasServlet
 */
@WebServlet("/ContadorVisitasServlet")
public class ContadorVisitasServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContadorVisitasServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Declarar el contador
		int contador = 0;
		//Revisar el arreglo de cookies
		Cookie[] c = request.getCookies();
		if(c != null) {
			for(Cookie coo : c) {
				if(coo.getName().equals("contadorVisitas")) {
					contador = Integer.parseInt(coo.getValue());
				}
			}
		}
		
		//Incrementar el contador
		contador++;
		//Agregamos la respuesta al navegador
		Cookie cookie = new Cookie("contadorVisitas", Integer.toString(contador));
		//Tiempo de la cookie
		cookie.setMaxAge(3600);
		response.addCookie(cookie);
		
		//Mandamos mensaje al navegador
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print("Numero de visitas de cada cliente: " + contador);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
