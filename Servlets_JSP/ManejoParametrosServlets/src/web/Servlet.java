package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {

	/**
	 * serialVersion
	 */
	private static final long serialVersionUID = -1226554461604979931L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Leer los parametros del formulario html
		resp.setContentType("text/html; charset=UTF-8"); 
		String usuario = req.getParameter("usuario");
		String password = req.getParameter("password");
		System.out.println(usuario);
		System.out.println(password);
		
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.print("<body>");
		out.print("El parametro usuario es: " + usuario);
		out.print("<br>");
		out.print("El parametro password es: " + password);
		out.print("<br/>");
		out.print("</body>");
		out.println("</html>");
		out.close();
		
	}
	

}
