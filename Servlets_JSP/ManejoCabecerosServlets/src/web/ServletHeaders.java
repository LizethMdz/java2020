package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletHeaders
 */
@WebServlet("/ServletHeaders")
public class ServletHeaders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletHeaders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8"); 
		PrintWriter out = response.getWriter();
		String methodHttp = request.getMethod();
		String Uri = request.getRequestURI();
		//Todos los cabeceros diponible:
		Enumeration cabeceros = request.getHeaderNames();
		
		out.println("<html>");
		out.println("<header>");
		out.println("<title>HEADERS</title>");
		out.println("</header>");
		out.println("<body>");
		out.println("<h1>Headers HTTP</h1>");
		out.println("<h1>Metodo:</h1>" + methodHttp);
		out.println("<h1>Uri:</h1>" + Uri);
		out.println("<h1>Cabeceros:</h1>");
		while(cabeceros.hasMoreElements()) {
			String nombreC = (String) cabeceros.nextElement();
			out.println("<b>*" + nombreC + "</b>");
			out.println(request.getHeader(nombreC));
		}
		out.println("</body>");
		out.println("</html>");
		out.close();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
