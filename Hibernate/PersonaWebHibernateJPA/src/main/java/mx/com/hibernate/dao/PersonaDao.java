package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.*;

import mx.com.hibernate.domain.Persona;

public class PersonaDao {
	private EntityManagerFactory emf;
	private EntityManager em;

	public PersonaDao() {
		emf = Persistence.createEntityManagerFactory("HibernateE");
		em = emf.createEntityManager();
	}

	public List<Persona> listar() {
		String hql = "SELECT  p FROM Persona p";
		Query q = em.createQuery(hql);
		List<Persona> p = q.getResultList();
		//for (Persona persona : p) {
		//	System.out.println("p: " + persona);
		//}
		return p;
	}

	public void insert(Persona persona) {
		try {
			// Abrir la transaccion
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(persona);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		// finally {
		// if(em != null) {
		// em.close();
		// }
		// }

	}

	public void update(Persona persona) {
		try {
			// Abrir la transaccion
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.merge(persona);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		// finally {
		// if(em != null) {
		// em.close();
		// }
		// }

	}

	public Persona searchById(Persona p) {
		return em.find(Persona.class, p.getIdPersona());
	}

	public void delete(Persona p) {
		try {
			// Abrir la transaccion
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(p));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
	}
}
