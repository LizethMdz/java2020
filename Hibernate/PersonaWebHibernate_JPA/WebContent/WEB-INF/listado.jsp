<%@taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listar Personas</title>
</head>
<body>
	<h1>Listar Personas</h1>
	<br><br>
	<ul>
		<c:forEach var="persona" items="${personas}">
			<li>${persona.id_persona} ${persona.nombre} ${persona.apellido} ${persona.saldo}</li>
		</c:forEach>
	</ul>
</body>
</html>