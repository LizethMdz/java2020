package mx.com.hibernate.servicio;

import java.util.List;

import mx.com.hibernate.dao.PersonaDao;
import mx.com.hibernate.domain.Persona;

public class ServicioPersonas {
	
	private PersonaDao personaDao;
	
	public ServicioPersonas() {
		this.personaDao = new PersonaDao();
	}
	
	public List<Persona> listarPersonas(){
		return this.personaDao.listar();
	}
}
