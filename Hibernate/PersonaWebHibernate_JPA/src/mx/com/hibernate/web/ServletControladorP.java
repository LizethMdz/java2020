package mx.com.hibernate.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.hibernate.domain.Persona;
import mx.com.hibernate.servicio.ServicioPersonas;

/**
 * Servlet implementation class ServletControladorP
 */
@WebServlet("/ServletControladorP")
public class ServletControladorP extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletControladorP() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServicioPersonas sPersonas = new ServicioPersonas();
		List<Persona> personas = sPersonas.listarPersonas();
		request.setAttribute("personas", personas);
		
		try {
		request.getRequestDispatcher("/WEB-INF/listado.jsp").forward(request, response);
		} catch (ServletException se) {
			se.printStackTrace(System.out);
		
		}catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
