package mx.com.hibernate.domain;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="persona")
public class Persona implements Serializable{
	
	/**
	 * SERIAL VERSION UID
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="id_persona")
	@Id
	private int idPersona;
	private String nombre;
	private String apellido;
	private String telefono;
	private String email;
	private Double saldo;
	
	
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", nombre=" + nombre + ", apellido=" + apellido + ", telefono="
				+ telefono + ", email=" + email + ", saldo=" + saldo + "]";
	}
	
	
	
	
}
