package mx.com.hibernate.test;

import java.util.List;

import javax.persistence.*;

import mx.com.hibernate.domain.Persona;

public class TestHibernate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String hql = "SELECT p FROM Persona p";
		//CONEXION A LA BASE DE DATOS
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("HibernateE");
		EntityManager entityManager = fabrica.createEntityManager();
		
		Query query = entityManager.createQuery(hql);
		List<Persona> personas = query.getResultList();
		
		for(Persona p: personas) {
			System.out.println("Persona: " + p);
		}
	}
	

}
