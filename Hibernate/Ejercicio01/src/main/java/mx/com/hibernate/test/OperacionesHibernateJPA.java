package mx.com.hibernate.test;

import mx.com.hibernate.dao.PersonaDao;
import mx.com.hibernate.domain.Persona;

public class OperacionesHibernateJPA {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonaDao personaDao = new PersonaDao();
		personaDao.listar();
		
		
		
		
		//Insertar
		Persona persona = new Persona();
		
		//Para el update,primero recuperarmos a la persona por su ID
		persona.setIdPersona(8);
		persona = personaDao.searchById(persona);
		System.out.println("Persona encontrada= " + persona);
		
		
		//persona.setNombre("Nancy");
		//persona.setApellido("Salazar");
		//persona.setEmail("nsalazar@mail.com");
		//persona.setTelefono("465465465");
		//persona.setSaldo(212.12);
		
		//personaDao.insert(persona);
		
		//personaDao.update(persona);
		personaDao.delete(persona);
		personaDao.listar();
	}

}
