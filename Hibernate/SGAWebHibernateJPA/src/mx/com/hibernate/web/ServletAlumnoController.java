package mx.com.hibernate.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.hibernate.domain.Alumno;
import mx.com.hibernate.servicio.ServicioAlumno;

/**
 * Servlet implementation class ServletAlumnoController
 */
@WebServlet("/ServletAlumnoController")
public class ServletAlumnoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAlumnoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServicioAlumno sa = new ServicioAlumno();
		
		List<Alumno> alumnos = sa.listarAlumnos();
		request.setAttribute("alumnos", alumnos);
		
		try {
		request.getRequestDispatcher("/WEB-INF/listarAlumnos.jsp").forward(request, response);
		} catch (ServletException se) {
			se.printStackTrace(System.out);
		
		}catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
