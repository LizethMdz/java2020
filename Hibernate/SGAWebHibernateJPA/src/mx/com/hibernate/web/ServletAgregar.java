package mx.com.hibernate.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.hibernate.domain.Alumno;
import mx.com.hibernate.domain.Contacto;
import mx.com.hibernate.domain.Domicilio;
import mx.com.hibernate.servicio.ServicioAlumno;

/**
 * Servlet implementation class ServletAgregar
 */
@WebServlet("/ServletAgregar")
public class ServletAgregar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String calle = request.getParameter("calle");
		String nocalle = request.getParameter("noCalle");
		String pais = request.getParameter("pais");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		
		Domicilio domicilio = new Domicilio();
        domicilio.setCalle(calle);
        domicilio.setNoCalle(nocalle);
        domicilio.setPais(pais);
        
        Contacto contacto = new Contacto();
        contacto.setEmail(email);
        contacto.setTelefono(telefono);
        
        Alumno alumno = new Alumno();
        alumno.setNombre(nombre);
        alumno.setApellido(apellido);
        alumno.setDomicilio(domicilio);
        alumno.setContacto(contacto);
        
        ServicioAlumno servicioAlumno = new ServicioAlumno();
        servicioAlumno.saveStudent(alumno);
        
        request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
