package mx.com.hibernate.test.ciclovida;

import javax.persistence.*;

import mx.com.hibernate.domain.Contacto;

public class Estado1Persistido {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("HibernateJpaSGA");
		EntityManager em = emf.createEntityManager();
		
		//1.Estado Transitivo
		Contacto contacto = new Contacto();
		contacto.setEmail("clara");
		contacto.setTelefono("879821213");
		
		//2.Estado persistente
		em.getTransaction().begin();
		
		em.persist(contacto);
		//Sincroniza la info en la BD
		em.flush();
		
		em.getTransaction().commit();
		
		//3. Detached (separado)
		System.out.println("Contacto: " + contacto);
	}
}
