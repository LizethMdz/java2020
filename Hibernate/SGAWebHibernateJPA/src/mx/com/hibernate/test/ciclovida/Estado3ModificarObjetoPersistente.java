package mx.com.hibernate.test.ciclovida;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.com.hibernate.domain.Contacto;

public class Estado3ModificarObjetoPersistente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("HibernateJpaSGA");
		EntityManager em = emf.createEntityManager();
		
		//Recuperar un objeto de tipo Contacto
		Contacto contacto = null;
		//Recuperamos el objeto
		//1.Transitivo
		contacto = em.find(Contacto.class, 3);
		//Se modifica pero no en la base de datos
		contacto.setEmail("clara@mail.com");
		
		em.getTransaction().begin();
		
		//2.Persistente
		//em.persist(contacto);
		em.merge(contacto);
		em.getTransaction().commit();
		
		//3.Detached
		System.out.println("Contacto: " + contacto);
	}

}
