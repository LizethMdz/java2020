package mx.com.hibernate.test.ciclovida;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.com.hibernate.domain.Contacto;

public class Estado2RecuperarObjetoPersistente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("HibernateJpaSGA");
		EntityManager em = emf.createEntityManager();
		
		//Recuperar un objeto de tipo Contacto
		Contacto contacto = null;
		//Iniciar la transaccion
		em.getTransaction().begin();
		//Recuperamos el objeto
		contacto = em.find(Contacto.class, 3);
		
		em.getTransaction().commit();
		
		//Detached
		System.out.println("Contacto: " + contacto);
	}

}
