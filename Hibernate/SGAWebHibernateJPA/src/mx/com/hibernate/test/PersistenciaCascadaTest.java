package mx.com.hibernate.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.com.hibernate.domain.Alumno;
import mx.com.hibernate.domain.Contacto;
import mx.com.hibernate.domain.Domicilio;

public class PersistenciaCascadaTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("HibernateJpaSGA");
		EntityManager em = emf.createEntityManager();
		
		Domicilio dom = new Domicilio();
		dom.setCalle("Loma");
		dom.setNoCalle("15.2");
		dom.setPais("Mexico");
		
		Contacto con = new Contacto();
		con.setEmail("clara@mail.com");
		con.setTelefono("54545454");
		
		Alumno alum = new Alumno();
		alum.setNombre("Catalina");
		alum.setApellido("Lara");
		alum.setDomicilio(dom);
		alum.setContacto(con);
		
		
		//Inicio Transaccion
		em.getTransaction().begin();
		//Estado persistente
		em.persist(alum);
		em.getTransaction().commit();
		
		System.out.println("Alumno: " + alum);
		
		
		
		
	}

}
