package mx.com.hibernate.test;

import java.util.List;

import mx.com.hibernate.dao.AlumnoDAO;
import mx.com.hibernate.dao.DomicilioDAO;

public class TestDAOs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AlumnoDAO alumnoDAO = new AlumnoDAO();
		System.out.println("Alumnos: ");
		imprimir(alumnoDAO.listar());
		
		DomicilioDAO domicilioDAO = new DomicilioDAO();
		System.out.println("Domicilios: ");
		imprimir(domicilioDAO.listar());
	}
	
	private static void imprimir(List coleccion) {
		for(Object o: coleccion) {
			System.out.println("valor: " + o);
		}
	}

}
