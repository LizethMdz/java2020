package mx.com.hibernate.servicio;

import java.util.List;

import mx.com.hibernate.dao.AlumnoDAO;
import mx.com.hibernate.domain.Alumno;

public class ServicioAlumno {
	
	AlumnoDAO alumnoDAO = new AlumnoDAO();
	
	public List<Alumno> listarAlumnos(){
		return this.alumnoDAO.listar();
	}
	
	public void saveStudent(Alumno alumno) {
		if(alumno != null && alumno.getIdAlumno() == null) {
			alumnoDAO.insert(alumno);
		}else {
			alumnoDAO.update(alumno);
		}
	}
	
	public void deleteStudent(Alumno alumno) {
		alumnoDAO.delete(alumno);
	}
	
	public Alumno findStudentById(Alumno alumno) {
		return alumnoDAO.searchById(alumno);
	}
}
