package mx.com.hibernate.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GenericDAO {
	protected EntityManagerFactory emf;
	protected EntityManager em;
	private static final String PU= "HibernateJpaSGA";
	
	
	public GenericDAO() {
		if(emf == null) {
			emf = Persistence.createEntityManagerFactory(PU);
		}
		
	}
	
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = emf.createEntityManager();
		}
		return em;
	}
	
	
}
