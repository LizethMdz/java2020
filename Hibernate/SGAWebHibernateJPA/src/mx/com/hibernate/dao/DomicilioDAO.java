package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.Query;

import mx.com.hibernate.domain.Domicilio;

public class DomicilioDAO extends GenericDAO{
	
	
	
	public List<Domicilio> listar() {
		String hql = "SELECT  d FROM Domicilio d";
		em = getEntityManager();
		Query q = em.createQuery(hql);
		return q.getResultList();
	}
	
	public void insert(Domicilio domicilio) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(domicilio);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void update(Domicilio domicilio) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.merge(domicilio);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void delete(Domicilio domicilio) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(domicilio));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public Domicilio searchById(Domicilio domicilio) {
		// Abrir la transaccion
		em = getEntityManager();
		return em.find(Domicilio.class, domicilio.getIdDomicilio());
	}
	
	

}
