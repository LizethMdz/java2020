package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.Query;

import mx.com.hibernate.domain.Contacto;

public class ContactoDAO extends GenericDAO{
	public List<Contacto> listar() {
		String hql = "SELECT  c FROM Contacto c";
		em = getEntityManager();
		Query q = em.createQuery(hql);
		return q.getResultList();
	}
	
	public void insert(Contacto contacto) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(contacto);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void update(Contacto contacto) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.merge(contacto);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void delete(Contacto contacto) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(contacto));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public Contacto searchById(Contacto contacto) {
		// Abrir la transaccion
		em = getEntityManager();
		return em.find(Contacto.class, contacto.getIdContacto());
	}
}
