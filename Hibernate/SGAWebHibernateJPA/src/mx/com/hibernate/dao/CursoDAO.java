package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.Query;

import mx.com.hibernate.domain.Curso;

public class CursoDAO extends GenericDAO{
	
	
	public List<Curso> listar() {
		String hql = "SELECT  c FROM Curso c";
		em = getEntityManager();
		Query q = em.createQuery(hql);
		return q.getResultList();
	}
	
	public void insert(Curso curso) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(curso);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void update(Curso curso) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.merge(curso);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void delete(Curso curso) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(curso));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public Curso searchById(Curso curso) {
		// Abrir la transaccion
		em = getEntityManager();
		return em.find(Curso.class, curso.getIdCurso());
	}
}
