package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.Query;

import mx.com.hibernate.domain.Alumno;

public class AlumnoDAO extends GenericDAO{

	
	public List<Alumno> listar() {
		String hql = "SELECT  a FROM Alumno a";
		em = getEntityManager();
		Query q = em.createQuery(hql);
		return q.getResultList();
	}
	
	public void insert(Alumno alumno) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(alumno);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void update(Alumno alumno) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.merge(alumno);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void delete(Alumno alumno) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(alumno));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public Alumno searchById(Alumno alumno) {
		// Abrir la transaccion
		em = getEntityManager();
		return em.find(Alumno.class, alumno.getIdAlumno());
	}
}
