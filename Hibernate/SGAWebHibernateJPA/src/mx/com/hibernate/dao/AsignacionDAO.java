package mx.com.hibernate.dao;

import java.util.List;

import javax.persistence.Query;

import mx.com.hibernate.domain.Asignacion;

public class AsignacionDAO extends GenericDAO{

	
	public List<Asignacion> listar() {
		String hql = "SELECT  a FROM Asignacion a";
		em = getEntityManager();
		Query q = em.createQuery(hql);
		return q.getResultList();
	}
	
	public void insert(Asignacion asignacion) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.persist(asignacion);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void update(Asignacion asignacion) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la informacion
			em.merge(asignacion);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public void delete(Asignacion asignacion) {
		try {
			// Abrir la transaccion
			em = getEntityManager();
			em.getTransaction().begin();
			// Guardamos la actualizacion de informacion
			em.remove(em.merge(asignacion));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			em.getTransaction().rollback();
		}
		finally {
			if(em != null) {
				em.close();
			}
		}

	}
	
	public Asignacion searchById(Asignacion asignacion) {
		// Abrir la transaccion
		em = getEntityManager();
		return em.find(Asignacion.class, asignacion.getIdAsignacion());
	}
}

