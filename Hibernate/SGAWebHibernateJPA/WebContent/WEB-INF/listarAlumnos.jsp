<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listar Alumnos</title>
</head>
<body>
	<h1>Listar Personas</h1>
	<br>
	<br>
	<a href="${pageContext.request.contextPath}/ServletAlumnoAgregar">Agregar Alumno</a>
	<ul>
		<c:forEach var="alumno" items="${alumnos}">
			
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">First</th>
						<th scope="col">Last</th>
						<th scope="col">Address</th>
						<th scope="col">Phone</th>
						<th scope="col">Email</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">
							<a href="${pageContext.request.contextPath}/ServletModificar?idAlumno=${alumno.idAlumno}">
                            ${alumno.idAlumno}
                        	</a>
						</th>
						<td>${alumno.nombre}</td>
						<td>${alumno.apellido}</td>
						<td>${alumno.domicilio.calle} ${alumno.domicilio.noCalle} ${alumno.domicilio.pais}</td>
						<td>${alumno.contacto.telefono}</td>
						<td>${alumno.contacto.email}</td>
					</tr>
					
				</tbody>
			</table>
		</c:forEach>
	</ul>

</body>
</html>