<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Agregar alumno</title>
</head>
<body>
	<h1>Agregar Personas</h1>
	<br>
	<br>
	<form action="${pageContext.request.contextPath}/ServletAgregar" method="post">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="input1">Nombre</label> <input type="text"
					class="form-control" id="input1" name="nombre">
			</div>
			<div class="form-group col-md-6">
				<label for="input2">Apellido</label> <input type="text"
					class="form-control" id="input2" name="apellido">
			</div>
			
			<div class="form-group col-md-6">
				<label for="input3">Calle</label> <input type="text"
					class="form-control" id="input3" name="calle">
			</div>
			
			<div class="form-group col-md-6">
				<label for="input4">No Calle</label> <input type="text"
					class="form-control" id="input4" name="noCalle">
			</div>
			
			<div class="form-group col-md-6">
				<label for="input5">Pais</label> <input type="text"
					class="form-control" id="input5" name="pais">
			</div>
			
			<div class="form-group col-md-6">
				<label for="input6">Email</label> <input type="email"
					class="form-control" id="input6" name="email">
			</div>
			
			<div class="form-group col-md-6">
				<label for=input7>Telefono</label> <input type="tel"
					class="form-control" id="input7" name="telefono">
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary">Registrar</button>
	</form>

</body>
</html>