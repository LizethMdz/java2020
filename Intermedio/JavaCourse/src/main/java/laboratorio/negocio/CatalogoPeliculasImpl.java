package laboratorio.negocio;

import java.util.List;

import laboratorio.datos.AccesoDatos;
import laboratorio.datos.AccesoDatosImpl;
import laboratorio.domain.Pelicula;
import laboratorio.excepciones.AccesoDEx;
import laboratorio.excepciones.LecturaDEx;

public class CatalogoPeliculasImpl implements CatalogoPeliculas{
	private final AccesoDatos datos;
	
	public CatalogoPeliculasImpl() {
		this.datos = new AccesoDatosImpl();
	}
	
	
	@Override
	public void agregarPelicula(String nombrePelicula, String nombreArchivo) {
		// TODO Auto-generated method stub
		Pelicula pelicula = new Pelicula(nombrePelicula);
		boolean anexar = false;
		try {
			anexar  = datos.existe(nombreArchivo);
			datos.escribir(pelicula, nombreArchivo, anexar);
		} catch (AccesoDEx e) {
			// TODO Auto-generated catch block
			System.out.println("Error de acceso a datos");
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void listarPeliculas(String nombreArchivo) {
		// TODO Auto-generated method stub
		try {
			List<Pelicula> catalogo = datos.listar(nombreArchivo);
			for(Pelicula pelicula: catalogo) {
				System.out.println("Pelicula: " + pelicula);
			}
		} catch (LecturaDEx e) {
			// TODO Auto-generated catch block
			System.out.println("Error de acceso a datos");
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public void buscarPelicula(String nombreArchivo, String buscar) {
		// TODO Auto-generated method stub
		String resultado = null;
		try {
			resultado = datos.buscar(nombreArchivo, buscar);
			System.out.println("Resultado encontrado: " + resultado);
		} catch (LecturaDEx e) {
			// TODO Auto-generated catch block
			System.out.println("Error al buscar la pelicula");
			e.printStackTrace();
		}
	}
	
	@Override
	public void iniciarArchivo(String nombreArchivo) {
		// TODO Auto-generated method stub
		try {
			if(datos.existe(nombreArchivo)) {
				datos.borrar(nombreArchivo);
				datos.crear(nombreArchivo);
			}else {
				datos.crear(nombreArchivo);
			}
		} catch (AccesoDEx e) {
			// TODO Auto-generated catch block
			System.out.println("Error de acceso a datos");
			e.printStackTrace();
		}
		
	}
}


