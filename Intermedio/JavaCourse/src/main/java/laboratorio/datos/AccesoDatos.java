package laboratorio.datos;

import java.util.List;

import laboratorio.domain.Pelicula;
import laboratorio.excepciones.*;

public interface AccesoDatos {
	
	boolean existe(String nomArchivo) throws AccesoDEx;
	
	public List<Pelicula> listar(String nombre) throws LecturaDEx;
	
	void escribir(Pelicula peli, String nomArchivo, boolean anexar) throws EscrituraDEx;
	
	public String buscar(String nomArchivo, String buscar) throws LecturaDEx;
	
	public void crear(String nombreArchivo) throws AccesoDEx;
	
	public void borrar(String nombreArchivo) throws AccesoDEx;

}
