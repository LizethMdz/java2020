package laboratorio.datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import laboratorio.domain.Pelicula;
import laboratorio.excepciones.AccesoDEx;
import laboratorio.excepciones.EscrituraDEx;
import laboratorio.excepciones.LecturaDEx;

public class AccesoDatosImpl implements AccesoDatos {
	
	@Override
	public boolean existe(String nomArchivo) throws AccesoDEx{
		File archivo = new File(nomArchivo);
		return archivo.exists();
	}
	
	@Override
	public List<Pelicula> listar(String nombre) throws LecturaDEx{
		File archivo = new File(nombre);
		List<Pelicula> catalogo = new ArrayList();
		try {
			BufferedReader entrada = new BufferedReader(new FileReader(archivo));
			String linea = null;
			linea = entrada.readLine();
			while(linea != null) {
				Pelicula pelicula = new Pelicula(linea);
				catalogo.add(pelicula);
				linea = entrada.readLine();
			}
			entrada.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return catalogo;
	}
	
	@Override
	public void escribir(Pelicula peli, String nomArchivo, boolean anexar) throws EscrituraDEx{
		File archivo = new File(nomArchivo);
		PrintWriter salida;
		try {
			salida = new PrintWriter(new FileWriter(archivo, anexar));
			String contenido = peli.toString();
			salida.println(contenido);
			salida.println();
			salida.close();
			System.out.println("Adding Movie Done!!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public String buscar(String nomArchivo, String buscar) throws LecturaDEx{
		File archivo = new File(nomArchivo);
		String resultado = null;
		try {
			BufferedReader entrada = new BufferedReader(new FileReader(archivo));
			String linea = null;
			int i = 0;
			linea = entrada.readLine();
			while(linea != null) {
				if(buscar != null && buscar.equalsIgnoreCase(linea)) {
					resultado = "Pelicula encontrada " + linea + " con ID de: " + i;
					break;
				}
				linea = entrada.readLine();
				i++;
			}
			entrada.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}
	
	@Override
	public void crear(String nombreArchivo) throws AccesoDEx{
		File archivo = new File(nombreArchivo);
		try {
			PrintWriter salida = new PrintWriter(archivo);
			salida.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void borrar(String nombreArchivo) throws AccesoDEx{
		File archivo = new File(nombreArchivo);
		archivo.delete();
		System.out.println("Deleting Catalog Done!!");
	}
}
