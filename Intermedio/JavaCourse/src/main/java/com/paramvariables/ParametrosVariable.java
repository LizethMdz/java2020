package com.paramvariables;

public class ParametrosVariable {

    static int contador;

    static {
        contador = 0;
        System.out.println("Tiene variable estaticas dentro de un Bloque estatico");
    }
    {
        System.out.println("Multiplicacion de matrices, Bloque no estatico");
    }


    public static void main(String[] args){
        imprimirNumeros(2,4,6,7,8,4);

        imprimirInformacion("Juan", true, 34,67,3,2,43);
    }

    public static void imprimirNumeros(int... numeros){
        for (int i = 0; i < numeros.length; i++) {
            System.out.println(numeros[i]);
        }

        for (int i : numeros) {
            System.out.println(i);
        }
    }

    public static void imprimirInformacion(String nombre, boolean valido, int... numeros){
        System.out.println(nombre);
        System.out.println(valido);

        for (int i : numeros) {
            System.out.println(i);
        }
    }
}