package com.interfases;

public interface AccesoDatos {

	// Modificadores public static final
	public static final int MAX_REGISTROS = 10;
	
	//Modificadores public and abstract
	
	public abstract void insertar();
	
	public abstract void listar();
	
	
}
