package com.abstractclass;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FiguraGeometrica tr = new Triangulo("Triangulo");
		System.out.println(tr);
		tr.dibujar();
		
		FiguraGeometrica circulo = new Circulo("Circulo");
		System.out.println(circulo);
		circulo.dibujar();
	}

}
