package com.objectclass;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Empleado emp1 = new Empleado("Juan", 100);
		System.out.println(emp1);
		Empleado emp2 = new Empleado("Juan", 100);
		System.out.println(emp2);
		
		compararObjetos(emp1, emp2);
		
	}
	
	private static void compararObjetos(Empleado emp, Empleado emp2) {
		System.out.println("Emp 1 " + emp.toString());
		System.out.println("Emp 2 " + emp2.toString());
		
		if(emp == emp2) {
			System.out.println("Los objetos tienen la misma direccion en memoria");
		
		}else {
			System.out.println("Los objetos tienen distintos valores en memoria");
		}
		
		//Revision equals
		if(emp.equals(emp2)) {
			System.out.println("Los objetos tienen el mismo contenido");
		
		}else {
			System.out.println("Los objetos tienen distinto contenido");
		}
		
		//Revision de hashcode
		if(emp.hashCode() == emp2.hashCode()) {
			System.out.println("Los objetos tienen el mismo codigo hash");
			System.out.println("HC1: " + emp.hashCode());
			System.out.println("HC2: " + emp2.hashCode());
		}else {
			System.out.println("Los objetos tienen distinto codigo hash");
		}
	}
}
