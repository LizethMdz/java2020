package com.instanceoof;

public class Empleado {
    protected String nombre;
    protected double sueldo;
    
    public Empleado(String nombre, double sueldo){
        this.nombre = nombre;
        this.sueldo = sueldo;
    }

    public String detalles(){
        return "Nombre " + this.nombre + " Sueldo " + this.sueldo;
    }

    public String getNombre(){
        return this.nombre;
    }

    public Double getSueldo(){
        return this.sueldo;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setSueldo(double sueldo){
        this.sueldo = sueldo;
    }
}