package com.instanceoof;

public class App {

    public static void main(String[] args){
        Empleado empl = new Empleado("Juan", 23);
        /*
        Comment
        */
        determinaTipo(empl);
        Gerente gr = new Gerente("Nadia", 123, "Recursos Humanos");
        determinaTipo(gr);

    }

    public static void determinaTipo(Empleado empleado){
        //System.out.println("Empleado: " + empleado.detalles());
        if (empleado instanceof Gerente){
            System.out.println("Gerente: " + ((Gerente) empleado).getDepartamento());
        }else if ( empleado instanceof Empleado){
            System.out.println("Empleado: " + empleado.getNombre());
        }
    }
    
}