package com.exceptions2;

public class ImplementacionOracle implements AccesoDatos{
	//NOTE: Si no se desean implementar los metodos se le agrega
	//abstract 
	private boolean simularErr;
	@Override
	public void insertar() throws AccesoDatosEx{
		// TODO Auto-generated method stub
		if(simularErr) {
			throw new EscrituraDtoEx("Error de lectura de datos");
		}else {
			System.out.println("Insertar desde oracle");
		}
	}

	@Override
	public void listar() throws AccesoDatosEx{
		// TODO Auto-generated method stub
		if(simularErr) {
			throw new EscrituraDtoEx("Error de lectura de datos");
		}else {
			System.out.println("Listar desde oracle");
		}
	}

	@Override
	public void simularError(boolean simularError) {
		// TODO Auto-generated method stub
		this.simularErr = simularError;
	}
	
	public boolean isSimularError() {
		return this.simularErr;
	}

}
