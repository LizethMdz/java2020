package com.exceptions2;

public interface AccesoDatos {

	// Modificadores public static final
	public static final int MAX_REGISTROS = 10;
	
	//Modificadores public and abstract
	
	public abstract void insertar() throws AccesoDatosEx;
	
	public abstract void listar() throws AccesoDatosEx;
	
	public abstract void simularError(boolean simularError);
	
}
