package com.exceptions2;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AccesoDatos datos = new ImplementacionMysql();
		datos.simularError(false);
		ejecutar(datos, "listar");
		
		AccesoDatos datos2 = new ImplementacionMysql();
		datos2.simularError(true);
		ejecutar(datos2, "insertar");
		

	}

	private static void ejecutar(AccesoDatos datos, String accion) {
		// TODO Auto-generated method stub
		if("listar".equals(accion)) {
			try {
				datos.listar();
			} catch (LecturaDtoEx ex) {
				// TODO Auto-generated catch block
				System.out.println("Error de lectura");
				ex.printStackTrace(System.out);
			}catch (AccesoDatosEx ex) {
				System.out.println("Error de acceso a datos");
				ex.printStackTrace(System.out);
			}catch (Exception ex ) {
				System.out.println("Error general");
				ex.printStackTrace(System.out);
			}
			
			finally {
				System.out.println("Se ejecuta, es opcional");
			}
		}else if("insertar".equals(accion)){
			try {
				datos.insertar();
			} catch (AccesoDatosEx ex) {
				// TODO Auto-generated catch block
				System.out.println("Error de acceso a datos");
				ex.printStackTrace(System.out);
			}
			
			finally {
				System.out.println("Se ejecuta, es opcional");
			}
		}
	}

}
