package com.exceptions2;

public class ImplementacionMysql implements AccesoDatos {
	private boolean simularErr;
	@Override
	public void insertar() throws AccesoDatosEx {
		// TODO Auto-generated method stub
		if(simularErr) {
			throw new EscrituraDtoEx("Error de escritura de datos");
		}else {
			System.out.println("Insertar desde mysql");
		}
		
	}

	@Override
	public void listar() throws AccesoDatosEx {
		// TODO Auto-generated method stubs
		if(simularErr) {
			throw new EscrituraDtoEx("Error de lectura de datos");
		}else {
			System.out.println("Listar desde mysql");
		}
		
	}

	@Override
	public void simularError(boolean simularError) {
		// TODO Auto-generated method stub
		this.simularErr = simularError;
	}
	
	public boolean isSimularError() {
		return this.simularErr;
	}

}
