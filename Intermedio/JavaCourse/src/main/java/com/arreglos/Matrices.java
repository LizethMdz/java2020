package com.arreglos;

import java.util.Random;

public class Matrices {
    
    public static void main(String[] arg){
        int edades [][] = new int[2][3];
        int anos [][] = new int [3][2];

        inicializar(edades);
        imprimirMatrices(edades);
        inicializar(anos);
        imprimirMatrices(anos);
        int [][] matrizSuma = sumaMatrices(edades, anos);

        if(matrizSuma == null){
            System.out.println("No se puede realizar la operacion");
        }else{
            imprimirMatrices(matrizSuma);
        }

        int [][] matrizMultiplicacion = multiplicacionMatrices(edades, anos);

        if(matrizMultiplicacion == null){
            System.out.println("No se puede realizar la operacion");
        }else{
            imprimirMatrices(matrizMultiplicacion);
        }

        
        
    }

    /**
     * Metodo que realiza la suma de Matrices
     * @param matriz
     * @param matriz2
     * @return matriz3 or null
     */
    public static int [][] sumaMatrices(int [][] matriz, int [][] matriz2){
        int [][] matriz3;
        int filas = matriz.length;
        int columnas =  matriz[0].length;
        int filas2 = matriz2.length;
        int columnas2 = matriz2[0].length;

        if (filas == filas2 && columnas == columnas2) {
            matriz3 = new int [filas][columnas2];
            for(int i = 0; i < filas; i++){
                for(int j = 0; j < columnas2; j++){
                    matriz3[i][j] = matriz[i][j] + matriz2[i][j];
                }
                
            }
            return matriz3;
        }else{
            return null;
        }
    }
    

    public static void imprimirMatrices(int [][] matriz){

        System.out.println("============ MATRIZ =============");
        System.out.println("\n");
        for(int i = 0; i < matriz.length; i++){
            for(int j = 0; j < matriz[i].length ; j++){
                System.out.print(" [ " + matriz[i][j] + " ] ");
            }
            System.out.println("\n");
        }
    }


    public static void inicializar(int [][] matriz){
        Random random = new Random();
        for(int i = 0; i < matriz.length; i++){
            for(int j = 0; j < matriz[i].length ; j++){
                matriz[i][j] = random.nextInt(10);
            }
        }
    }

    public static int [][] multiplicacionMatrices(int [][] matriz, int [][] matriz2){
        int [][] matriz3;
        int filas = matriz.length;
        int columnas =  matriz[0].length;
        int filas2 = matriz2.length;
        int columnas2 = matriz2[0].length;

        if (columnas == filas2) {
            matriz3 = new int [filas][columnas2];
            for(int i = 0; i < filas; i++){
                for(int j = 0; j < columnas2; j++){
                    for(int k=0; k < columnas; k++){
                        matriz3[i][j] += matriz[i][k] * matriz2[k][j];      
                    }
                }
                
            }
            return matriz3;
        }else{
            return null;
        }
    }
    
    
}