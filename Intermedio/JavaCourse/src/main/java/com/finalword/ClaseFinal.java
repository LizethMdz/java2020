package com.finalword;

public final class ClaseFinal {
    //Final en atributos
    public final int var = 4;
    //Final en constantes
    public static final int VAR_PRIMITIVO = 4;

    public static final Persona VAR_OBJETO = new Persona();

    //Final en metodos
    //No se puede modificar en una clase Hija

    public final int suma(int a , int b){
        return a + b;
    }
}