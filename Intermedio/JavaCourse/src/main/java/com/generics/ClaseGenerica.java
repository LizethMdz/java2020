package com.generics;

public class ClaseGenerica<T> {
	T objeto;
	//Definir constructor
	public ClaseGenerica(T objeto) {
		this.objeto = objeto;
	}
	
	public void obtenerTipo() {
		System.out.println("El tipo es: " + objeto.getClass().getSimpleName());
	}
}
