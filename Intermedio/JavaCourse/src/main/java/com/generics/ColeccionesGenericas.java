package com.generics;

import java.util.*;

public class ColeccionesGenericas {
	public static void main(String[] args) {
		List<String> lista = new ArrayList();
		lista.add("100");
		lista.add("200");
		lista.add("300");
		
		imprimir(lista);
		
		Set<String> miSet = new HashSet();
		
		miSet.add("10");
		miSet.add("20");
		miSet.add("30");
		//No se agrega
		miSet.add("30");
		
		imprimir(miSet);
		
		
		Map<String, String> miMapa = new HashMap();
		//llave, valor
		miMapa.put("1", "Paris");
		miMapa.put("2", "Belgica");
		miMapa.put("3", "Italia");
		//No se agrega un nueva llave, sino que reemplaza su valor
		miMapa.put("3", "Libano");
		//Imprimir llaves
		imprimir(miMapa.keySet());
		//Imprimir valores
		imprimir(miMapa.values());
	}
	
	private static void imprimir(Collection<String> coleccion) {
		for(Object elemento: coleccion) {
			System.out.println("Elemento: " + elemento);
		}
	}
}
