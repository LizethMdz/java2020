package com.generics;

public class ManejoGenericosTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClaseGenerica<Integer> objetoInt = new ClaseGenerica(15);
		objetoInt.obtenerTipo();
		
		/*
		 * La cual sabe de que tipo es 
		 * hasta que se le declara. 
		 * Tienen restricciones, ya que, 
		 * no pueden ser 
		 * utilizados con datos primitivos.
		 * 
		 * */
		ClaseGenerica<String> objetoString = new ClaseGenerica("sadasd");
		objetoString.obtenerTipo();
	}

}
