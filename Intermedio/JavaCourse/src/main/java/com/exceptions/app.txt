Excepciones:Situacion no esperada durante la ejecucion

Checkexceptions - Heredan de la clase exception

SQLException

Uncheck exception

RuntimeException
		|
NullPointerException


Sintaxis del manejo de excepciones

public void verfica(){
	try{
		//CODIGO QUE LANZA EXCEPCIONES
	}catch (Exception ex) {
		//BLOQUE DE CODIGO QUE MANEJA LA EXCEPCION
		ex.printStackTrace();
	}
	
	finally{
		//BLOQUE DE CODIGO OPCIONAL, Y SIEMPRE SE EJECUTA
	}
}

==============================================

STACKTRACE - Bitacora de errores en la aplicacion

Mecanismo de pila excepciones.

Metodo que arroja el error -> Si no atrapa la excepcion, se propaga ->

Metodo main, si no atrapa la excepcion, la propaga, terminando mal.

====================================================================

