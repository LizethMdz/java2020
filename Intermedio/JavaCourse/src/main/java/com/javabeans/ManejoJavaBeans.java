package com.javabeans;

public class ManejoJavaBeans {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonaBean bean = new PersonaBean();
		bean.setNombre("Maria");
		bean.setEdad(28);
		
		System.out.println("Nombre: " + bean.getNombre());
		System.out.println("Edad: " + bean.getEdad());
		
		PersonaBean bean2 = new PersonaBean("Hugo", 78);
		System.out.println(bean2);
	}

}
