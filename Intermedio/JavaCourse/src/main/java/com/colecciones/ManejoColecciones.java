package com.colecciones;

import java.util.*;

public class ManejoColecciones {
	public static void main(String[] args) {
		List miLista = new ArrayList();
		miLista.add("1 Hola");
		miLista.add(2);
		miLista.add(3);
		//Se agrega
		miLista.add(3);
		
		imprimir(miLista);
		
		Set miSet = new HashSet();
		
		miSet.add("100");
		miSet.add("200");
		miSet.add("300");
		//No se agrega
		miSet.add("300");
		
		imprimir(miSet);
		
		Map miMapa = new HashMap();
		//llave, valor
		miMapa.put("1", "Paris");
		miMapa.put("2", "Belgica");
		miMapa.put("3", "Italia");
		//No se agrega un nueva llave, sino que reemplaza su valor
		miMapa.put("3", "Libano");
		//Imprimir llaves
		imprimir(miMapa.keySet());
		//Imprimir valores
		imprimir(miMapa.values());
	
	}
	
	private static void imprimir(Collection coleccion) {
		for(Object elemento: coleccion) {
			System.out.println("Elemento: " + elemento);
		}
	}
}
