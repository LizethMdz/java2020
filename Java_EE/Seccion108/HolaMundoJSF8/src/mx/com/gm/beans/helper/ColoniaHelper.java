package mx.com.gm.beans.helper;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import mx.com.gm.beans.model.Colonia;

@RequestScoped
@Named
public class ColoniaHelper {
	public List<Colonia> getColonias(){
		
		List<Colonia> colonias = new ArrayList<>();
		
		int coloniaId = 1;
		Colonia colonia = new Colonia(coloniaId++, "Napoles", 3254);
		colonias.add(colonia);
		colonia = new Colonia(coloniaId++, "Polanco", 546);
		colonias.add(colonia);
		colonia = new Colonia(coloniaId++, "Santa Clara", 5654);
		colonias.add(colonia);
		
		return colonias;
	}
	
	public int getColoniaByName(String nombreCol) {
		int coloniaId  =0;
		List<Colonia> colonias = this.getColonias();
		
		for(Colonia col: colonias) {
			if(col.getNombreColonia().equals(nombreCol)) {
				coloniaId = col.getColoniaId();
				break;
			}
		}
		
		return coloniaId;
	}
	
	public int getColoniaByCode(int codigoPostal) {
		int coloniaId  = 0;
		List<Colonia> colonias = this.getColonias();
		
		for(Colonia col: colonias) {
			if(col.getCodigoPostal() == codigoPostal) {
				coloniaId = col.getColoniaId();
				break;
			}
		}
		
		return coloniaId;
	}
	
	public List<SelectItem> getSelectItems(){
		List<SelectItem> selectItems = new ArrayList<>();
		List<Colonia> colonias = this.getColonias();
		
		for(Colonia col: colonias) {
			SelectItem selectItem = new SelectItem(col.getColoniaId(), col.getNombreColonia());
			
			selectItems.add(selectItem);
		}
		
		return selectItems;
	}
}
