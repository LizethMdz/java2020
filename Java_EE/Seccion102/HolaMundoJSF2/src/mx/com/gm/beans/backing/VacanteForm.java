package mx.com.gm.beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import mx.com.gm.beans.model.Candidato;

@Named
@RequestScoped
public class VacanteForm {
	//Injeccion del objeto candidato
	
	@Inject
	private Candidato candidato;
	
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	
	public String enviar() {
		if(this.candidato.getNombre().equals("Juan")) {
			return "exito";
		}else {
			return "fallo";
		}
	}
}
