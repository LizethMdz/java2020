package mx.com.gm.cliente.ClienteSGAWS;

import java.util.List;

import mx.com.gm.clientews.servicio.Persona;
import mx.com.gm.clientews.servicio.PersonaServiceImplService;
import mx.com.gm.clientews.servicio.PersonaServiceWS;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	PersonaServiceWS personaServicio = new PersonaServiceImplService().getPersonaServiceImplPort();
        System.out.println( "Llamando al WS Personas" );
        
        List<Persona> personas = personaServicio.listarPersonas();
        for(Persona persona: personas) {
        	System.out.println( "Persona desplegada: " + "Id: " + persona.getIdPersona() +  " Nombre: " +
        			persona.getNombre() + " Apellido: " + persona.getApellido());
        }
        
        
        System.out.println( "Fin del llamado al WS Sumar" );
    }
}
