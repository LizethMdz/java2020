package mx.com.gm.cliente.ClienteSumaWS;

import mx.com.gm.clientews.servicio.ServicioSumarImplService;
import mx.com.gm.clientews.servicio.ServicioSumarWS;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       
        ServicioSumarWS servicioSumar = new ServicioSumarImplService().getServicioSumarImplPort();
        System.out.println( "Llamando al WS Sumar" );
        int x = 6;
        int y = 3;
        
        int resultado = servicioSumar.sumar(x, y);
        
        System.out.println( "Resultado: " + resultado );
        System.out.println( "Fin del llamado al WS Sumar" );
    }
}
