package mx.com.gm.beans.SumaWebService;
import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless
@WebService(endpointInterface="mx.com.gm.beans.SumaWebService.ServicioSumarWS")
public class ServicioSumarImpl implements ServicioSumarWS {

	public int sumar(int a, int b) {
		// TODO Auto-generated method stub
		return a + b ;
	}

}
