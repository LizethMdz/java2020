package mx.com.gm.sga.cliente.asociaciones;

import java.util.List;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.gm.sga.domain.Persona;
import mx.com.gm.sga.domain.Usuario;

public class ClienteAsociaciones {
	static Logger log = LogManager.getRootLogger();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
        EntityManager em = emf.createEntityManager();
        
        List<Persona> personas = em.createNamedQuery("Persona.findAll").getResultList();
        
        em.close();
        
        //Imprimir los objetos de tipo persona
        for(Persona persona: personas) {
        	log.debug("Persona: " + persona);
        	//Recuperamos los usuarios de las personas
        	for(Usuario usuario: persona.getUsuarios()) {
        		log.debug("Usuario: " + usuario);
        	}
        }
	}

}
