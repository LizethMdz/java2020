package mx.com.gm.beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.gm.beans.model.Candidato;

@Named
@RequestScoped
public class VacanteForm {
	//Injeccion del objeto candidato
	
	@Inject
	private Candidato candidato;
	
	Logger log = LogManager.getRootLogger();
	
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	
	public String enviar() {
		if(this.candidato.getNombre().equals("Juan")) {
			log.info("Caso de exito");
			return "exito";
		}else {
			log.info("Caso de fallo");
			return "fallo";
		}
	}
}
