package mx.com.gm.sga.cliente.ciclovida;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.gm.sga.domain.Persona;

public class EliminarObjetoJPA {
	 static Logger log = LogManager.getRootLogger();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
        EntityManager em = emf.createEntityManager();

        //Inicia la transaccion
        //Paso 1. Iniciar una transaccion
        EntityTransaction tx = em.getTransaction();
        tx.begin();

       //Paso 2. Ejecuta SQL de tipo select
       //El id proporcionado debe existir en la base de datos
       Persona persona1 = em.find(Persona.class, 10);
       
       
       //Paso 3. Termina la transaccion 1
       tx.commit();
       
       //Objeto en estado detached
       log.debug("Objeto recuperado:" + persona1);
       
       //Paso 4. Inicia transaccion 2
       EntityTransaction tx2 = em.getTransaction();
       tx2.begin();
       
       //Paso 5. Ejecuta el SQL que es un delete
       em.remove(em.merge(persona1));
       
       //Paso 6. Termina tx2
       tx2.commit();
       
       //objeto en estado detached ya eliminado
       log.debug("Objeto eliminado:" + persona1);
       
       //Cerramos el entity manager
       em.close();

	}

}
