package mx.com.gm.sga.cliente;

import javax.naming.*;
import org.apache.logging.log4j.*;

import mx.com.gm.sga.domain.Persona;
import mx.com.gm.sga.servicio.PersonaServiceRemote;

public class PruebaManejoTransacciones {
	static Logger log = LogManager.getRootLogger();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* JNI Para conectarnos remotamente
		 2020-07-15T12:07:05.610-0500|Información: Portable JNDI names for EJB UsuarioDaoImpl: [java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/UsuarioDaoImpl, java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/UsuarioDaoImpl!mx.com.gm.sga.datos.UsuarioDao]
		 2020-07-15T12:07:05.649-0500|Información: Portable JNDI names for EJB PersonaDaoImpl: [java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/PersonaDaoImpl!mx.com.gm.sga.datos.PersonaDao, java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/PersonaDaoImpl]
		 2020-07-15T12:07:05.740-0500|Información: Portable JNDI names for EJB UsuarioServiceImpl: [java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/UsuarioServiceImpl!mx.com.gm.sga.servicio.UsuarioService, java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/UsuarioServiceImpl!mx.com.gm.sga.servicio.UsuarioServiceRemote]
		 2020-07-15T12:07:05.806-0500|Información: Portable JNDI names for EJB PersonaServiceImpl: [java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaService, java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaServiceRemote]
		 * */
		try {

			Context jndi = new InitialContext();
            PersonaServiceRemote personaService = (PersonaServiceRemote) jndi.lookup("java:global/SGA_WebEJB8-0.0.1-SNAPSHOT/PersonaServiceImpl!mx.com.gm.sga.servicio.PersonaServiceRemote");
			
			log.debug("Iniciando prueba manejo transaccional Persona Service");
			 //Buscar un objeto persona
            Persona persona1 = personaService.encontrarPersonaPorId(new Persona(1));
            
            log.debug("Persona recuperada:" + persona1);
            
            //Cambiar el apellido persona
            //persona1.setApellido("cambio con error....................................................................................");
            persona1.setApellido("Jimenez");
            personaService.modificarPersona(persona1);
            log.debug("Objeto modificado:" + persona1);
            log.debug("Fin prueba EJB transaccional");



		} catch (NamingException e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			e.printStackTrace(System.out);
		}
	}

}
