package mx.com.gm.beans.model;

import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Named
@RequestScoped
public class Candidato {
	private String nombre;
	private String apellido;
	private int salario;
	private Date fechaNacimiento;
	
	
	
	public Candidato() {
		log.info("Creando candidato....");
		this.setNombre("Introduce tu nombre");
	}

	Logger log = LogManager.getRootLogger();

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
		log.info("Modificando la propiedad nombre: " + this.nombre);
	}
	

	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
		log.info("Modificando la propiedad apellido: " + this.apellido);
	}



	public int getSalario() {
		return salario;
	}



	public void setSalario(int salario) {
		this.salario = salario;
		log.info("Modificando la propiedad salario: " + this.salario);
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	

	
}
