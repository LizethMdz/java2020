package mx.com.gm.beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.gm.beans.model.Candidato;

@Named
@RequestScoped
public class VacanteForm {
	//Injeccion del objeto candidato
	
	@Inject
	private Candidato candidato;
	
	Logger log = LogManager.getRootLogger();
	
	
	
	public VacanteForm() {
		log.info("Creando el objeto VacanteForm");
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	
	public String enviar() {
		if(this.candidato.getNombre().equals("Juan")) {
			if(this.candidato.getApellido().equals("Perez")) {
				String msg = "Ya trabaja aqui XD";
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				String componentId = null; //mensaje global
				facesContext.addMessage(componentId, facesMessage);
				return "index";
			}
			log.info("Caso de exito");
			return "exito";
		}else {
			log.info("Caso de fallo");
			return "fallo";
		}
	}
}
