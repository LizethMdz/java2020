package mx.com.javaee.cliente;

import java.util.List;
import javax.naming.*;
import mx.com.javaee.domain.Persona;
import mx.com.javaee.service.PersonaServiceRemote;

public class ClientePersonaService {
    public static void main(String[] args) {
        System.out.println("Iniciando llamada al EJB desde el cliente\n");
        try {
            Context jndi = new InitialContext();
            PersonaServiceRemote personaService = (PersonaServiceRemote) jndi.lookup("java:global/SGA_EJB/PersonaServiceImpl!mx.com.javaee.service.PersonaServiceRemote");
            
            List<Persona> personas = personaService.listarPersonas();
            
            for(Persona persona: personas){
                System.out.println(persona);
            }
            
            System.out.println("\nFin llamada al EJB desde el cliente");
            
        } catch (NamingException ex) {
           ex.printStackTrace(System.out);
        }
    }
    
}
