package mx.com.javaee.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import mx.com.javaee.domain.Persona;

@Stateless
public class PersonaServiceImpl implements PersonaServiceRemote{

	public List<Persona> listarPersonas() {
		// TODO Auto-generated method stub
		List<Persona> personas = new ArrayList<Persona>();
        personas.add(new Persona(1,"Juan","Perez","jperez@mail.com","55139900"));
        personas.add(new Persona(2,"Martha","Suarez","msuarez@mail.com","55899910"));
        return personas;
	}

	public Persona encontrarPersonaPorId(Persona persona) {
		// TODO Auto-generated method stub
		return null;
	}

	public Persona encontrarPersonaPorEmail(Persona persona) {
		// TODO Auto-generated method stub
		return null;
	}

	public void registrarPersona(Persona persona) {
		// TODO Auto-generated method stub
		
	}

	public void modificarPersona(Persona persona) {
		// TODO Auto-generated method stub
		
	}

	public void eliminarPersona(Persona persona) {
		// TODO Auto-generated method stub
		
	}

	

    
}
