package mx.com.javaee.test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import mx.com.javaee.beans.HolaMundoEJBRemote;

public class TestEJB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Iniciando EJB desde el cliente........");
		try {
			Context jndi = new InitialContext();
			HolaMundoEJBRemote holaMEJBR= (HolaMundoEJBRemote) jndi.lookup("java:global/HolaMundoEJB/HolaMundoEJBImpl!mx.com.javaee.beans.HolaMundoEJBRemote");
			int res = holaMEJBR.sumar(3, 8);
			System.out.println("Resultado........ " + res);
		}catch(NamingException n) {
			n.printStackTrace(System.out);
		}
	}

}
