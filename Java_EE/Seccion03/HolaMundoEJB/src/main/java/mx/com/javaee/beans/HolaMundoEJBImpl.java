package mx.com.javaee.beans;

import javax.ejb.Stateless;

@Stateless
public class HolaMundoEJBImpl implements HolaMundoEJBRemote{

	public int sumar(int a, int b) {
		// TODO Auto-generated method stub
		System.out.println("Ejecutando el metodo..........");
		return a + b;
	}
	
	//Portable JNDI names for EJB HolaMundoEJBImpl: [java:global/HolaMundoEJB-0.0.1-SNAPSHOT/HolaMundoEJBImpl, java:global/HolaMundoEJB-0.0.1-SNAPSHOT/HolaMundoEJBImpl!mx.com.javaee.beans.HolaMundoEJBRemote]

}
