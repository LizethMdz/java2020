package mx.com.gm.beans.ciclovida;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DebuggerListener implements PhaseListener{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	Logger log = LogManager.getRootLogger();

	@Override
	public void afterPhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub
		if(log.isInfoEnabled()) {
			log.info("Despues de la fase: " + arg0.getPhaseId().toString());
		}
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub
		if(log.isInfoEnabled()) {
			log.info("Antes de la fase: " + arg0.getPhaseId().toString());
		}
	}

	@Override
	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.ANY_PHASE;
	}

}
