package mx.com.gm.sga.web;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.gm.sga.domain.Usuario;
import mx.com.gm.sga.servicio.UsuarioService;

/**
 * Servlet implementation class UsuarioServlet
 */
@WebServlet("/usuarios")
public class UsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Inject
    UsuarioService usuarioService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 List<Usuario> usuarios = usuarioService.listarUsuarios();
	     System.out.println("usuarios:" + usuarios);
         request.setAttribute("usuarios", usuarios);
         request.getRequestDispatcher("/listadoUsuarios.jsp").forward(request, response);
	}


}
