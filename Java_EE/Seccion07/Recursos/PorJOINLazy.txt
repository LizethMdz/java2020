18:19:06 [main] DEBUG org.jboss.logging - Logging Provider: org.jboss.logging.Log4j2LoggerProvider
18:19:06 [main] INFO  org.hibernate.validator.internal.util.Version - HV000001: Hibernate Validator 6.0.10.Final
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.resolver.TraversableResolvers - Found javax.persistence.Persistence on classpath containing 'getPersistenceUtil'. Assuming JPA 2 environment. Trying to instantiate JPA aware TraversableResolver
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.resolver.TraversableResolvers - Instantiated JPA aware TraversableResolver of type org.hibernate.validator.internal.engine.resolver.JPATraversableResolver.
18:19:06 [main] DEBUG org.hibernate.validator.internal.xml.ValidationXmlParser - Trying to load META-INF/validation.xml for XML based Validator configuration.
18:19:06 [main] DEBUG org.hibernate.validator.internal.xml.ResourceLoaderHelper - Trying to load META-INF/validation.xml via TCCL
18:19:06 [main] DEBUG org.hibernate.validator.internal.xml.ResourceLoaderHelper - Trying to load META-INF/validation.xml via Hibernate Validator's class loader
18:19:06 [main] DEBUG org.hibernate.validator.internal.xml.ValidationXmlParser - No META-INF/validation.xml found. Using annotation based configuration only.
18:19:06 [main] DEBUG org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator - Loaded expression factory via original TCCL
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.ValidatorFactoryImpl - HV000234: Using org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator as ValidatorFactory-scoped message interpolator.
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.ValidatorFactoryImpl - HV000234: Using org.hibernate.validator.internal.engine.resolver.JPATraversableResolver as ValidatorFactory-scoped traversable resolver.
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.ValidatorFactoryImpl - HV000234: Using org.hibernate.validator.internal.util.ExecutableParameterNameProvider as ValidatorFactory-scoped parameter name provider.
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.ValidatorFactoryImpl - HV000234: Using org.hibernate.validator.internal.engine.DefaultClockProvider as ValidatorFactory-scoped clock provider.
18:19:06 [main] DEBUG org.hibernate.validator.internal.engine.ValidatorFactoryImpl - HV000234: Using org.hibernate.validator.internal.engine.scripting.DefaultScriptEvaluatorFactory as ValidatorFactory-scoped script evaluator factory.
[EL Info]: 2020-07-14 18:19:06.972--ServerSession(1541046463)--EclipseLink, version: Eclipse Persistence Services - 2.7.0.v20170811-d680af5
[EL Info]: connection: 2020-07-14 18:19:07.228--ServerSession(1541046463)--/file:/D:/lizet/Documents/JAVA/java2020/Java_EE/Seccion07/SGA_WebEJB6/target/classes/_SgaPU login successful
18:19:07 [main] DEBUG  - 
1. Consulta de todas las Personas
[EL Fine]: sql: 2020-07-14 18:19:07.257--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona, APELLIDO, EMAIL, NOMBRE, SALDO, TELEFONO FROM PERSONA
18:19:07 [main] DEBUG  - 
2. consulta de la Persona con id = 1
[EL Fine]: sql: 2020-07-14 18:19:07.288--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona, APELLIDO, EMAIL, NOMBRE, SALDO, TELEFONO FROM PERSONA WHERE (NOMBRE = ?)
	bind => [1 parameter bound]
18:19:07 [main] DEBUG  - 
4. Consulta de datos individuales, se crea un arreglo (tupla) de tipo object de 3 columnas
[EL Fine]: sql: 2020-07-14 18:19:07.296--ServerSession(1541046463)--Connection(1613332278)--SELECT NOMBRE, APELLIDO, EMAIL FROM PERSONA
18:19:07 [main] DEBUG  - 
5. Obtiene el objeto Persona y el id, se crea un arreglo de tipo Object con 2 columnas
[EL Fine]: sql: 2020-07-14 18:19:07.3--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona, APELLIDO, EMAIL, NOMBRE, SALDO, TELEFONO, id_persona FROM PERSONA
. Consulta de todas las personas
[EL Fine]: sql: 2020-07-14 18:19:07.304--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona FROM PERSONA

7. Regresa el valor minimo y maximo del idPersona (scaler result)
[EL Fine]: sql: 2020-07-14 18:19:07.31--ServerSession(1541046463)--Connection(1613332278)--SELECT MIN(id_persona), MAX(id_persona), COUNT(id_persona) FROM PERSONA
18:19:07 [main] DEBUG  - 
8. Cuenta los nombres de las personas que son distintos
[EL Fine]: sql: 2020-07-14 18:19:07.314--ServerSession(1541046463)--Connection(1613332278)--SELECT COUNT(DISTINCT(NOMBRE)) FROM PERSONA
18:19:07 [main] DEBUG  - 
9. Concatena y convierte a mayusculas el nombre y apellido
[EL Fine]: sql: 2020-07-14 18:19:07.317--ServerSession(1541046463)--Connection(1613332278)--SELECT CONCAT(CONCAT(NOMBRE, ?), APELLIDO) FROM PERSONA
	bind => [1 parameter bound]
18:19:07 [main] DEBUG  - 
10. Obtiene el objeto persona con id igual al parametro proporcionado
18:19:07 [main] DEBUG  - 
11. Obtiene las personas que contengan una letra a en el nombre, sin importar si es mayusculas o minuscula
[EL Fine]: sql: 2020-07-14 18:19:07.321--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona, APELLIDO, EMAIL, NOMBRE, SALDO, TELEFONO FROM PERSONA WHERE UPPER(NOMBRE) LIKE UPPER(?)
	bind => [1 parameter bound]
18:19:07 [main] DEBUG  - 
12. Uso de between
[EL Fine]: sql: 2020-07-14 18:19:07.324--ServerSession(1541046463)--Connection(1613332278)--SELECT id_persona, APELLIDO, EMAIL, NOMBRE, SALDO, TELEFONO FROM PERSONA WHERE (id_persona BETWEEN ? AND ?)
	bind => [2 parameters bound]
18:19:07 [main] DEBUG  - 
13. Uso del ordenamiento
18:19:07 [main] DEBUG  - 
14. Uso de subquery
18:19:07 [main] DEBUG  - 
15. Uso de join con lazy loading
[EL Fine]: sql: 2020-07-14 18:19:07.328--ServerSession(1541046463)--Connection(1613332278)--SELECT t1.id_usuario, t1.PASSWORD, t1.USERNAME, t1.id_persona FROM PERSONA t0, USUARIO t1 WHERE (t0.id_persona = t1.id_persona)
18:19:07 [main] DEBUG  - Usuario [idUsuario=1, password=123, username=juanperez, persona=Persona [idPersona=1, apellido=Juarez, email=j.juarez@mail.com, nombre=Juan Manuel, saldo=100.0, telefono=443523187]]
18:19:07 [main] DEBUG  - Usuario [idUsuario=2, password=123, username=mgarcia, persona=Persona [idPersona=2, apellido=Garcia, email=mgarcia@gmail.com, nombre=Marco Adrian, saldo=500.0, telefono=54684665]]
18:19:07 [main] DEBUG  - Usuario [idUsuario=3, password=123, username=hhernandez, persona=Persona [idPersona=11, apellido=Hernandez, email=hhernandez@mail.com, nombre=Hugo, saldo=45.2, telefono=55778822]]