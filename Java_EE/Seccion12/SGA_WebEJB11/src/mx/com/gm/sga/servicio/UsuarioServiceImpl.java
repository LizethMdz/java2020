package mx.com.gm.sga.servicio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import mx.com.gm.sga.datos.UsuarioDao;
import mx.com.gm.sga.domain.Usuario;

@Stateless
public class UsuarioServiceImpl implements UsuarioServiceRemote, UsuarioService{

	@Inject
	private UsuarioDao usuarioDao;
	
	@Override
	public List<Usuario> listarUsuarios() {
		// TODO Auto-generated method stub
		 return usuarioDao.findAllUsuarios();
	}

	@Override
	public Usuario encontrarUsuarioPorId(Usuario usuario) {
		// TODO Auto-generated method stub
		 return usuarioDao.findUsuarioById(usuario);
	}

	@Override
	public void registrarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		 usuarioDao.insertUsuario(usuario);
	}

	@Override
	public void modificarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		 usuarioDao.updateUsuario(usuario);
	}

	@Override
	public void eliminarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		 usuarioDao.deleteUsuario(usuario);
	}

}
