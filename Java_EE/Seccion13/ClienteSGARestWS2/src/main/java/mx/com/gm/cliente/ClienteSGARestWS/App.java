package mx.com.gm.cliente.ClienteSGARestWS;

import java.util.List;
import javax.ws.rs.client.*;
import javax.ws.rs.core.*;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import mx.com.gm.cliente.domain.Persona;

/**
 * Hello world!
 *
 */
public class App {
	// Variable a utilizar
	private static final String URL_BASE = "http://localhost:8080/SGA_WebEJB12/webservice";
	private static Client cliente;
	private static WebTarget webTarget;
	private static Persona persona;
	private static List<Persona> personas;
	private static Invocation.Builder invocationBuilder;
	private static Response response;

	public static void main(String[] args) {
		System.out.println("Test Rest Service!");
		
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basicBuilder().nonPreemptive().credentials("admin", "admin").build();
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(feature);

		cliente = ClientBuilder.newClient(clientConfig);
		// Leer una persona
		webTarget = cliente.target(URL_BASE).path("/personas");
		// Proporcionamos un idPersona valido
		persona = webTarget.path("/1").request(MediaType.APPLICATION_XML).get(Persona.class);
		System.out.println("Persona recuperada:" + persona);
		// Leer todas las personas (metodo get con readEntity de tipo List<>
		personas = webTarget.request(MediaType.APPLICATION_XML).get(Response.class)
				.readEntity(new GenericType<List<Persona>>() {
				});
		System.out.println("\nPersonas recuperadas:");
		imprimirPersonas(personas);
		
		//Agregar una persona (metodo post)
        Persona nuevaPersona = new Persona();
        nuevaPersona.setNombre("Jose");
        nuevaPersona.setApellido("Garcia");
        nuevaPersona.setEmail("jgarcia@mail.com");
        nuevaPersona.setSaldo(45.78);
        nuevaPersona.setTelefono("8432189");
        
        invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        response = invocationBuilder.post(Entity.entity(nuevaPersona, MediaType.APPLICATION_XML));
        System.out.println("");
        System.out.println(response.getStatus());
        //Recuperamos la personas recien agregada para despues modificarla y al final eliminarla
        Persona personaRecuperada = response.readEntity(Persona.class);
        System.out.println("Persona agregada:" + personaRecuperada);
        
        //Modificar la persona (metodo put)
        //persona recuperada anteriormente
        Persona personaModificar = personaRecuperada;
        personaModificar.setApellido("Moncada");
        String pathId = "/" + personaModificar.getIdPersona();
        invocationBuilder = webTarget.path(pathId).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.put(Entity.entity(personaModificar, MediaType.APPLICATION_XML));
        
        System.out.println("");
        System.out.println("response:" + response.getStatus());
        System.out.println("Persona modifica:" + response.readEntity(Persona.class));
        
        //eliminar una persona
        //persona recuperada anteriormente
        Persona personaEliminar = personaRecuperada;
        String pathEliminarId = "/" + personaEliminar.getIdPersona();
        invocationBuilder = webTarget.path(pathEliminarId).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.delete();
        System.out.println("");
        System.out.println("response:" + response.getStatus());
        System.out.println("Persona Eliminada" + personaEliminar);


	}
	
	 private static void imprimirPersonas(List<Persona> personas) {
	        for(Persona p: personas){
	            System.out.println("Persona:" + p);
	        }
	    }

}
