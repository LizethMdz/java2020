package mx.com.gm.cliente.ClienteSGARestWS;

import javax.ws.rs.client.*;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import mx.com.gm.cliente.domain.Persona;

public class PruebaRestWS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basicBuilder().nonPreemptive().credentials("admin", "admin").build();
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(feature);
		
		Client cliente = ClientBuilder.newClient(clientConfig);
		
		WebTarget webTarget = cliente.target("http://localhost:8080/SGA_WebEJB12/webservice").path("/personas");
		Persona persona = webTarget.path("/1").request(MediaType.APPLICATION_XML).get(Persona.class);
		System.out.println("Persona recuperada:" + persona);
	}

}
