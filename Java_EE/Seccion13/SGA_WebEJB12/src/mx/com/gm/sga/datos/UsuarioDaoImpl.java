package mx.com.gm.sga.datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.com.gm.sga.domain.Usuario;

@Stateless 
public class UsuarioDaoImpl implements UsuarioDao{
	
	@PersistenceContext(unitName="SgaPU")
    EntityManager em;

	@Override
	public List<Usuario> findAllUsuarios() {
		// TODO Auto-generated method stub
		return em.createNamedQuery("Usuario.findAll").getResultList();
	}

	@Override
	public Usuario findUsuarioById(Usuario usuario) {
		// TODO Auto-generated method stub
		return em.find(Usuario.class, usuario.getIdUsuario());
	}

	@Override
	public void insertUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		 em.persist(usuario);
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		em.merge(usuario);
	}

	@Override
	public void deleteUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		em.remove(em.merge(usuario));
	}

}
