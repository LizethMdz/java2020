package mx.com.gm.sga.domain;

import java.io.Serializable;

//Se agrega el serializable, ya que esta clase pasara por la web
public class Persona implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Integer idPersona;
    private String apellido;
    private String email;
    private Double saldo;
    private String telefono;
    
    public Persona(){
        
    }
    
    public Persona(Integer idPersona){
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", apellido=" + apellido + ", email=" + email + ", saldo=" + saldo
				+ ", telefono=" + telefono + "]";
	}

}
