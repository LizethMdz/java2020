package mx.com.gm.beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.com.gm.beans.model.Candidato;

@Named
@RequestScoped
public class VacanteForm {
	//Injeccion del objeto candidato
	
	@Inject
	private Candidato candidato;
	
	private boolean comentarioEnviado = true;
	
	Logger log = LogManager.getRootLogger();
	
	
	
	public VacanteForm() {
		log.info("Creando el objeto VacanteForm");
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	
	public void ocultarComentario(ActionEvent actionEvent ) {
		if(this.comentarioEnviado) {
			this.comentarioEnviado = !this.comentarioEnviado;
		}else {
			this.comentarioEnviado = true;
		}
	}
	
	
	public boolean isComentarioEnviado() {
		return comentarioEnviado;
	}

	public void setComentarioEnviado(boolean comentarioEnviado) {
		this.comentarioEnviado = comentarioEnviado;
	}

	public String enviar() {
		if(this.candidato.getNombre().equals("Juan")) {
			if(this.candidato.getApellido().equals("Perez")) {
				String msg = "Ya trabaja aqui XD";
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				String componentId = null; //mensaje global
				facesContext.addMessage(componentId, facesMessage);
				return "index";
			}
			log.info("Caso de exito");
			return "exito";
		}else {
			log.info("Caso de fallo");
			return "fallo";
		}
	}
	
	public void codPostalListener(ValueChangeEvent valueChangeEvent) {
		FacesContext fC = FacesContext.getCurrentInstance();
		UIViewRoot uiViewRoot = fC.getViewRoot();
		//Obtiene un cambio en la entrada del input con un evento
		String nCodPostal = (String) valueChangeEvent.getNewValue();
		//Ejemplo de codigos
		if("76903".equals(nCodPostal)) {
			//Recupera el valor
			UIInput coloniaIdInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:colonia");
			String newColonia = "Napoles";
			coloniaIdInputText.setValue(newColonia);
			coloniaIdInputText.setSubmittedValue(nnewColonia);
			
			UIInput ciudadInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:ciudad");
			String newCiudad = "Mexico";
			ciudadInputText.setValue(newCiudad);
			ciudadInputText.setSubmittedValue(newCiudad);
			
			fC.renderResponse();
		}
	}

}
