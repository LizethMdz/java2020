package com.arreglos;

public class Arreglos {
    public static void main(String[] args){
        int [] edades;

        edades = new int[3];
        edades[0] = 23;
        edades[1] = 12;
        edades[2] = 212;

        for(int i=0; i < edades.length; i++){
            System.out.println(edades[i]);
        }

        Persona [] personas = new Persona[4];

        personas[0] = new Persona("Karla");
        personas[1] = new Persona("Romeo");
        personas[2] = new Persona("Julieta");
        personas[3] = new Persona("Montesqueo");

        for(int i=0; i < personas.length; i++){
            System.out.println(personas[i].getNombre());
        }
    }
    
    
}