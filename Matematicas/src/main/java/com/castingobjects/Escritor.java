package com.castingobjects;

public class Escritor extends Empleado {

    final TipoEscritura tipoEscritura;

    public Escritor(String nombre, double sueldo, TipoEscritura tipoEscritura) {
        super(nombre, sueldo);
        this.tipoEscritura = tipoEscritura;
        
    }

    @Override
    public String obtenerDetalles(){
        return super.obtenerDetalles() + " Tipo Escritura: " + tipoEscritura.getDescripcion();
    }

    public TipoEscritura getTipoEscritura(){
        return this.tipoEscritura;
    }

    public String getTipoEscrituraTexto(){
        return tipoEscritura.getDescripcion();
    }
    
}