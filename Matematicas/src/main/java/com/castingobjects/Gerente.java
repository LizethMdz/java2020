package com.castingobjects;

public class Gerente extends Empleado {
    protected String departamento;

    public Gerente(String nombre, double sueldo, String departamento) {
        super(nombre, sueldo);
        this.departamento = departamento;
    }

    public String obtenerDetalles(){
        return super.obtenerDetalles() + " Departamento: " + departamento;
    }

    public String getDepartamento(){
        return this.departamento;
    }

    public void setDepartamento(String departamento){
        this.departamento = departamento;
    }
    
}