package com.castingobjects;


public enum TipoEscritura {
    CLASICO("ESCRITURA A MANO"),
    DIGITAL("ESCRITURA DIGITAL");

    private final String descripcion;

    private TipoEscritura(String descripcion){
        this.descripcion = descripcion;
    }

    public String getDescripcion(){
        return this.descripcion;
    }
}