package com.castingobjects;

public class Test {
    public static void main(String [] args){
        Empleado empleado;

        empleado = new Escritor("Lola", 2140, TipoEscritura.CLASICO);
        System.out.println(empleado.obtenerDetalles());

        Escritor escritor = (Escritor) empleado;
        System.out.println(escritor.getTipoEscrituraTexto());

        System.out.println(((Escritor) empleado).getTipoEscrituraTexto());

        empleado = new Gerente("Nadia", 855, "Sistema");
        System.out.println(((Gerente) empleado).getDepartamento());
    }
}