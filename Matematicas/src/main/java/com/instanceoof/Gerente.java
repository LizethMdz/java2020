package com.instanceoof;

public class Gerente extends Empleado {

    private String departamento;

    public Gerente(String nombre, double sueldo, String departamento) {
        super(nombre, sueldo);
        // TODO Auto-generated constructor stub
        this.departamento = departamento;
    }

    @Override
    public String detalles(){
        /**Esta linea se puede simplificar  */
        //return "Nombre " + this.nombre + " Sueldo " + this.sueldo + " Departamento " + this.departamento;
        /**Utilizacion de palabra super */
        return super.detalles() + " Departamento: "+this.departamento; 
    }

    public String getDepartamento(){
        return this.departamento;
    }

    public void setDepartamento(String departamento){
        this.departamento = departamento;
    }
    
}