package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dominio.Cliente;
import datos.Conexion;


public class ClienteDaoJDBC {
	private static final String SQL_SELECT = "SELECT id_cliente, nombre, apellido, email, telefono, saldo FROM clientes";
	private static final String SQL_SELECT_BY_ID = "SELECT id_cliente, nombre, apellido, email, telefono, saldo FROM clientes WHERE id_cliente=?";
	
	private static final String SQL_INSERT = "INSERT INTO clientes(nombre, apellido, email, telefono, saldo) VALUES(?,?,?,?,?)";
	private static final String SQL_UPDATE = "UPDATE clientes SET nombre=?, apellido=?, email=?, telefono=?, saldo=? WHERE id_cliente=?" ;
	private static final String SQL_DELETE = "DELETE FROM clientes WHERE id_cliente=?";
	
	public List<Cliente> select(){
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Cliente persona = null;
		List<Cliente> personas = new ArrayList<Cliente>();
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_SELECT);
			rs = st.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("id_cliente");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido");
				String email = rs.getString("email");
				String telefono = rs.getString("telefono");
				Double saldo = rs.getDouble("saldo");
				
				persona = new Cliente(id, nombre, apellido, email, telefono, saldo);
				
				personas.add(persona);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			Conexion.close(rs);
			Conexion.close(st);
			Conexion.close(conn);
		}
		return personas;
	}
	
	public Cliente encontrarCliente(Cliente cliente) {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_SELECT_BY_ID);
			st.setInt(1, cliente.getId_cliente());
			rs = st.executeQuery();
			rs.absolute(1); //Primer registro
			
			// Almacenamos los datos en variables
			String nombre = rs.getString("nombre");
			String apellido = rs.getString("apellido");
			String email = rs.getString("email");
			String telefono = rs.getString("telefono");
			Double saldo = rs.getDouble("saldo");
			// Asignamos los valores a ese cliente
			cliente.setNombre(nombre);
			cliente.setApellido(apellido);
			cliente.setEmail(email);
			cliente.setTelefono(telefono);
			cliente.setSaldo(saldo);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			Conexion.close(rs);
			Conexion.close(st);
			Conexion.close(conn);
		}
		return cliente;
	}
	
	
	public int insert(Cliente persona) {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_INSERT);
			st.setString(1,  persona.getNombre());
			st.setString(2,  persona.getApellido());
			st.setString(3,  persona.getEmail());
			st.setString(4,  persona.getTelefono());
			st.setDouble(5, persona.getSaldo());
			
			System.out.print("Ejecutando query: " + SQL_INSERT);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		return rows;
	}
	
	
	public int update(Cliente persona) {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_UPDATE);
			st.setString(1,  persona.getNombre());
			st.setString(2,  persona.getApellido());
			st.setString(3,  persona.getEmail());
			st.setString(4,  persona.getTelefono());
			st.setDouble(5, persona.getSaldo());
			st.setInt(6, persona.getId_cliente());
			
			System.out.print("Ejecutando query: " + SQL_UPDATE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		
		return rows;
		
	}
	
	public int delete(Cliente persona) {
		Connection conn = null;
		PreparedStatement st = null;
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			st = conn.prepareStatement(SQL_DELETE);
			st.setInt(1, persona.getId_cliente());
			System.out.println("Ejecutando query: " + SQL_DELETE);
			rows = st.executeUpdate();
			System.out.println("Registros afectados: " + rows);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			Conexion.close(st);
			Conexion.close(conn);
		}
		
		return rows;
		
	}
	
}
