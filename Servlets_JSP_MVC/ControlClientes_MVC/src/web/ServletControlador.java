package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ClienteDaoJDBC;
import dominio.Cliente;

/**
 * Servlet implementation class ServletControlador
 */
@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletControlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String accion = request.getParameter("accion");
		if(accion != null) {
			switch(accion) {
				case "editar":
					this.editarCliente(request, response);
					break;
				case "eliminar":
					this.eliminarCliente(request, response);
					break;
				default:
					this.accionDefault(request, response);
			}
		}else {
			this.accionDefault(request, response);
		}
	}
	
	private void accionDefault(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Cliente> clientes = new ClienteDaoJDBC().select();
		System.out.println("clientes = " + clientes);
		HttpSession sesion = request.getSession();
		sesion.setAttribute("clientes", clientes);
		sesion.setAttribute("totalClientes", clientes.size());
		sesion.setAttribute("saldoTotal", this.calcularSaldoTotal(clientes));
		//request.getRequestDispatcher("clientes.jsp").forward(request, response);
		response.sendRedirect("clientes.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String accion = request.getParameter("accion");
		if(accion != null) {
			switch(accion) {
				case "insertar":
					this.insertarCliente(request, response);
					break;
				case "modificar":
					this.modificarCliente(request, response);
					break;
				default:
					this.accionDefault(request, response);
			}
		}else {
			this.accionDefault(request, response);
		}
	}
	
	private double calcularSaldoTotal(List<Cliente> clientes) {
		double saldoTotal =0;
		for(Cliente cliente: clientes) {
			saldoTotal += cliente.getSaldo();
		}
		return saldoTotal;
	}
	
	private void insertarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		String saldo = request.getParameter("saldo");
		double saldoT = 0;
		
		if(saldo != null && !"".equals(saldo)) {
			saldoT = Double.parseDouble(saldo);
		}
		
		//Crear el objeto Cliente
		Cliente cliente = new Cliente(nombre, apellido, telefono, email, saldoT);
		
		//Insertarlo en la base de datos
		int resModificados = new ClienteDaoJDBC().insert(cliente);
		System.out.println("Registros Modificados" + resModificados);
		//Redireccionar a la accion por default
		this.accionDefault(request, response);
	}

	private void editarCliente (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Recuperar el idCliente
		int idCliente = Integer.parseInt(request.getParameter("idCliente"));
		//	Buscar y almacenar un cliente en base a su ID
		Cliente cliente = new ClienteDaoJDBC().encontrarCliente(new Cliente(idCliente));
		request.setAttribute("cliente", cliente);
		String jspEditar = "/WEB-INF/paginas/cliente/editarCliente.jsp";
		request.getRequestDispatcher(jspEditar).forward(request, response);
	}
	
	private void modificarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Recuperamos los valores del formulario editarCliente
		int idCliente = Integer.parseInt(request.getParameter("idCliente"));
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		String saldo = request.getParameter("saldo");
		double saldoT = 0;
		
		if(saldo != null && !"".equals(saldo)) {
			saldoT = Double.parseDouble(saldo);
		}
		
		//Crear el objeto Cliente
		Cliente cliente = new Cliente(idCliente,nombre, apellido, telefono, email, saldoT);
		
		//Insertarlo en la base de datos
		int resModificados = new ClienteDaoJDBC().update(cliente);
		System.out.println("Registros Modificados" + resModificados);
		
		//Redireccionar a la accion por default
		this.accionDefault(request, response);
	}
	
	private void eliminarCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Recuperamos los valores del formulario editarCliente
		int idCliente = Integer.parseInt(request.getParameter("idCliente"));
		
		//Crear el objeto Cliente
		Cliente cliente = new Cliente(idCliente);
		
		//Insertarlo en la base de datos
		int resModificados = new ClienteDaoJDBC().delete(cliente);
		System.out.println("Registros Modificados" + resModificados);
		
		//Redireccionar a la accion por default
		this.accionDefault(request, response);
	}
}
