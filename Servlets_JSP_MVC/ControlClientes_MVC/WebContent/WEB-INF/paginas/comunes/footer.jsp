<footer class="bg-light text-white mt-5 p-5" id="pie-pagina">
	<div class="container">
		<div class="row text-muted">
			<p>Welcome!</p>
		</div>
		<p class="text-muted">
			www.lamt.com @Copyright <a th:href="www.lamt.com">Lamt SA de CV</a>
		</p>
		<span class="text-muted">Place sticky footer content here.</span>
	</div>
</footer>