<section id="actions" class="py-4 mb-4">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<!-- Button Edicion -->
				<a href="index.jsp" class="btn btn-dark">
					<i class="fas fa-arrow-left"></i> Regresar a Inicio
				</a>
			</div>
			
			<div class="col-md-3">
				<!-- Button trigger modal -->
				<button type="submit" class="btn btn-secondary">
					<i class="fas fa-check"></i> Guardar Cliente 
				</button>
			</div>
			
			<div class="col-md-3">
				<!-- Button trigger modal -->
				<a href="${pageContext.request.contextPath}/ServletControlador?accion=eliminar&idCliente=${cliente.id_cliente}" class="btn btn-secondary">
					<i class="fas fa-trash"></i> Eliminar Cliente 
				</a>
			</div>
		</div>
	</div>
</section>
