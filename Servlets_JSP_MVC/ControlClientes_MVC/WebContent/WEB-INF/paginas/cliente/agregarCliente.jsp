<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Agregar Clientes</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="${pageContext.request.contextPath}/ServletControlador?accion=insertar" method="post" class="was-validated">
				<div class="modal-body">

					<div class="form-row">
						<!-- <input type="hidden" name="idPersona"> -->
						<div class="form-group col-md-12">
							<label for="nombre"></label>Nombre<input
								type="text" class="form-control" name="nombre" id="nombre"
								required="true">
						</div>
						<div class="form-group col-md-12">
							<label for="apellido"></label>Apellido<input
								type="text" class="form-control" name="apellido" id="apellido"
								required="true">

						</div>
						<div class="form-group col-md-12">
							<label for="email"></label>Telefono<input
								type="email" class="form-control" name="email" id="email"
								required="true" placeholder="example@email.com">

						</div>
						<div class="form-group col-md-12">
							<label for="telefono"></label>Telefono<input
								type="tel" class="form-control" name="telefono" id="telefono"
								required="true">
						</div>
						<div class="form-group col-md-12">
							<label for="saldo"></label>Saldo<input
								type="number" class="form-control" name="saldo" id="saldo"
								step="any" required="true">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-outline-dark btn-block">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

