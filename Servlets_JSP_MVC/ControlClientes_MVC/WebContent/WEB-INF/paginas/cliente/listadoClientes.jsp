<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- LIBRERIA PARA FORMATO DE MONEDA -->
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_MX"/>

<section id="clientes">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
			
				<div class="card">
					<div class="card-header">
						Listado de Clientes
					</div>
					<div class="table-responsive">
					
						<table class="table text-center">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Nombre</th>
						      <th scope="col">Saldo</th>
						      <th scope="col">Acciones</th>
						    </tr>
						  </thead>
						  <tbody>
						  <c:forEach var="cliente" items="${clientes}" varStatus="status">
						    <tr>
						      <!-- <th scope="row">${cliente.id_cliente}</th>-->
						      <th scope="row">${status.count}</th>
						      <td>${cliente.nombre} ${cliente.apellido}</td>
						      <td><fmt:formatNumber value="${cliente.saldo}" type="currency"/></td>
						      <td>
						      	<a href="${pageContext.request.contextPath}/ServletControlador?accion=editar&idCliente=${cliente.id_cliente}" class="btn btn-dark"><i class="fas fa-edit"></i>Editar</a>
						      </td>
						    </tr>
						    </c:forEach>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- TOTALES -->
			<div class="col-md-3">
				<div class="card text-center bg-light mb-3">
					<div class="card-body">
						<h3>Saldo</h3>
						<h6 class="display-4">
							<i class="fas fa-users"></i> 
							<fmt:formatNumber value="${saldoTotal}" type="currency"></fmt:formatNumber>
						</h6>
					</div>
				</div>
				<div class="card text-center bg-dark mb-3">
					<div class="card-body text-white">
						<h3>Total Clientes</h3>
						<h6 class="display-4">
							<i class="fas fa-users text-white"></i>
							${totalClientes}
						</h6>
					</div>
				</div>
			</div>
			<!-- / TOTALES -->
		</div>
	</div>
</section>

<!-- Agregar de Clientes -->
<jsp:include page="/WEB-INF/paginas/cliente/agregarCliente.jsp"/>

