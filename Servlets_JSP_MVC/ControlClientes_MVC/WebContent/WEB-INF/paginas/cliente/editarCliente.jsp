<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Editar Clientes</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/a42df98ebc.js" crossorigin="anonymous"></script>
<body>
	
	<!--Cabecero -->
	<jsp:include page="/WEB-INF/paginas/comunes/header.jsp"/>
	<!-- Formulario -->
	<form action="${pageContext.request.contextPath}/ServletControlador?accion=modificar&idCliente=${cliente.id_cliente}" method="post" class="was-validated">
		<!-- Botones Edicion -->
		<jsp:include page="/WEB-INF/paginas/comunes/botonesNavEdicion.jsp"/>
		<section id="details">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								Editar Cliente
							</div>
							<div class="card-body">
								<div class="form-row">
									<!-- <input type="hidden" name="idPersona"> -->
									<div class="form-group col-md-12">
										<label for="nombre"></label>Nombre<input
											type="text" class="form-control" name="nombre" id="nombre" value="${cliente.nombre}"
											required="true">
									</div>
									<div class="form-group col-md-12">
										<label for="apellido"></label>Apellido<input
											type="text" class="form-control" name="apellido" id="apellido" value="${cliente.apellido}"
											required="true">
									</div>
									<div class="form-group col-md-12">
										<label for="email"></label>Telefono<input
											type="email" class="form-control" name="email" id="email" value="${cliente.email}"
											required="true" placeholder="example@email.com">
			
									</div>
									<div class="form-group col-md-12">
										<label for="telefono"></label>Telefono<input
											type="tel" class="form-control" name="telefono" id="telefono" value="${cliente.telefono}"
											required="true">
									</div>
									<div class="form-group col-md-12">
										<label for="saldo"></label>Saldo<input
											type="number" class="form-control" name="saldo" id="saldo" value="${cliente.saldo}"
											step="any" required="true">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form>
	
	
	<div class="container">
		<!-- Editar Clientes -->
		
			
	</div>
	<!-- Pie de Pagnina -->
	<jsp:include page="/WEB-INF/paginas/comunes/footer.jsp"/>
	
</head>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<body>

</body>
</html>