NOTE MVC - PERMITE SEPARAR LA VISTA, CONTROLADOR Y EL MODELO

-LOS SERVLETS ESTAN ENFOCADOS EN CONTROLAR EL FLUJO DE LA PETICION HTTP

-LOS JSP'S ESTAN ENFOCADOS EN DESPLEGAR LA INFORMACION DE LA APLICACION WEB

-LA INFORMACION QUE SE COMPARTE ENTRE LOS CONMPONENTES(SERVLETS Y JSP'S) SUELE MANEJAR JAVA BEAN

-EL PATRON MVC NOS PERMITE INTEGRAR LOS JSP'S, A LOS SERVLETS Y LOS JAVA BEANS

====================================================================================================

EJEMPLOS DE FRAMEWORKS QUE L EMPLEAN:

JSP/SERVLETS: SE IMPLEMENTA MANUALMENTE CON AYUDA DEL OBJETO REQUESTDISPATCHER PARA EL FLUJO DE LA APP

STRUTS - DE APACHE, UTILIZA JSPS CON TAGS DE STRUTS, ACTIONFORM (MODELO), ACTION(CONTROLADOR), ENTRE OTROS

JAVASERVER FACES (JSF): ES UNA TECNOLOGIA QUE UTILIZA CONCEPTOS COMO JSPS CON TAGS DE JSF, MANAGEDBEAN(CONTROLADOR)
Y JAVABEANS (MODELO)

SPRINGMVC - ES UNA EXTENSION DEL FRAMEWORK DE SPRING, QUE UTILIZA JSP CON TAGS DE SPRING, CLASES JAVA(CONTROLADORES)
Y JAVABEANS(MODELO).

=============================================================================================================================

NOTE: ARQUITECTURA MVC CON JSP'S Y SERVLETS

FORMULARIO HTML -> SERVLET(CONTROLADOR) -> JAVABEANS(MODELO) -> JSP(VISTA) -> RESULTADO(HTML, PDF, ETC)

=============================================================================================================================

NOTE: PASOS GENERALES

 SERVLET(CONTROLADOR) -> JAVABEANS(MODELO) -> JSP(VISTA) -> RESULTADO(HTML, PDF, ETC)

 1. PROCESAMOS Y VALIDAMOS LOS PARAMETROS (SI APLICA)
    request.getParameter("nombreParam");
 2. REALIZAMOS LA LOGICA DE PRESENTACION ALAMACENANDO EL RESULTADO EN JAVABEANS
    Rectangulo rec = new Rectangulo();

  3. COMPARTIMOS EL OBJETO A UTILIZAR EN ALGUN ALCANCE (SCOPE)
    request.setAttribute("recBean", rec);

  4. REDIRECCIONAMIENTO AL JSP SELECCIONADO
     RequestDispatcher dispatcher = request.getRequestDispatcher("resultado.jsp");
     dispatcher.forward(request, response);

================================================================================================================

REINSTALAR EL DATA TOOLS PLATFORM

LINK AL REPOSITORIO

URL: https://download.eclipse.org/datatools/updates/1.14.100.201805031905/repository/


================================================

*LIBRERIAS pom.xml

<dependencies>
  	<!-- https://mvnrepository.com/artifact/javax.persistence/javax.persistence-api -->
	<dependency>
	    <groupId>javax.persistence</groupId>
	    <artifactId>javax.persistence-api</artifactId>
	    <version>2.2</version>
	</dependency>
  	<!-- https://mvnrepository.com/artifact/javax/javaee-api -->
	<dependency>
	    <groupId>javax</groupId>
	    <artifactId>javaee-api</artifactId>
	    <version>8.0.1</version>
	    <scope>provided</scope>
	</dependency>
  	<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
	<dependency>
	    <groupId>mysql</groupId>
	    <artifactId>mysql-connector-java</artifactId>
	    <version>8.0.20</version>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-dbcp2 -->
	<dependency>
	    <groupId>org.apache.commons</groupId>
	    <artifactId>commons-dbcp2</artifactId>
	    <version>2.7.0</version>
	</dependency>
</dependencies>
