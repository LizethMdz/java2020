<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Despliegue de Variables</title>
</head>
<body>
	<h1>Despliegue de variables </h1>
	Variable en alcance request: ${mensaje} 
	<br><br>
	Variable en alcance de session: 
	<br><br>
	Base ${recBean.base}
	<br><br>
	Altura ${recBean.altura}
	<br><br>
	Area ${recBean.area}
	<br><br>
	<a href="${pageContext.request.contextPath}/index.jsp">
		Regresar
	</a>
</body>
</html>