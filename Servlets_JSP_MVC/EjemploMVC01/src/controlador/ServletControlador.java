package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Rectangulo;

/**
 * Servlet implementation class ServletControlador
 */
@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletControlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 1. PROCESAMOS Y VALIDAMOS LOS PARAMETROS (SI APLICA)
	   
	    // 2. REALIZAMOS LA LOGICA DE PRESENTACION ALAMACENANDO EL RESULTADO EN JAVABEANS / CREAR LOS JB
	       Rectangulo rec = new Rectangulo(3,6);

	    // 3. COMPARTIMOS EL OBJETO A UTILIZAR EN ALGUN ALCANCE (SCOPE)
	       request.setAttribute("mensaje", "Saludos desde el servlet");
	       HttpSession sesion = request.getSession();
	       sesion.setAttribute("recBean", rec);

	    // 4. REDIRECCIONAMIENTO AL JSP SELECCIONADO
	        RequestDispatcher dispatcher = request.getRequestDispatcher("vista/desplegarVariables.jsp");
	        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
