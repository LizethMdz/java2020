package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Rectangulo;

/**
 * Servlet implementation class ServletControlador
 */
@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletControlador() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 1. PROCESAMOS Y VALIDAMOS LOS PARAMETROS (SI APLICA)
		String accion = request.getParameter("accion");

		// 2. REALIZAMOS LA LOGICA DE PRESENTACION ALAMACENANDO EL RESULTADO EN
		// JAVABEANS / CREAR LOS JB
		Rectangulo recRequest = new Rectangulo(87, 6);
		Rectangulo recSession = new Rectangulo(465, 132);
		Rectangulo recApplication = new Rectangulo(31, 98);

		// 3. COMPARTIMOS EL OBJETO A UTILIZAR EN ALGUN ALCANCE (SCOPE)
		
		if("agregaVariables".equals(accion)) {
			//Request
			request.setAttribute("recRequest", recRequest);
			//Session
			HttpSession sesion = request.getSession();
			sesion.setAttribute("recSession", recSession);
			//Application
			ServletContext application = this.getServletContext();
			application.setAttribute("recApplication", recApplication);
			
			//Mensaje
			request.setAttribute("mensaje", "Fueron agregadas exitosamente");
			
			// 4. REDIRECCIONAMIENTO AL JSP SELECCIONADO
			request.getRequestDispatcher("index.jsp").forward(request, response);
			
		}else if("listarVariables".equals(accion)) {
			// 4. REDIRECCIONAMIENTO AL JSP SELECCIONADO
			request.getRequestDispatcher("WEB-INF/alcanceVariables.jsp").forward(request, response);

		}else {
			//Mensaje
			request.setAttribute("mensaje", "Accion no proporcionada");
			// 4. REDIRECCIONAMIENTO AL JSP SELECCIONADO
			request.getRequestDispatcher("index.jsp").forward(request, response);
			//response.sendRedirect("index.jsp") - NO PROPAGA LOS OBJETOS REQUEST AND RESPONSE
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
