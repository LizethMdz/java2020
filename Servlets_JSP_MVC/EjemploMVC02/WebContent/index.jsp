<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejemplo MVC</title>
</head>
<body>
	<h1>Ejemplo MVC 02</h1>
	<br><br>
	<div style="color:red">${mensaje}</div>
	<a href="${pageContext.request.contextPath}/ServletControlador">
		Link al servlet controlador sin parametros
	</a>
	<br><br>
	<a href="${pageContext.request.contextPath}/ServletControlador?accion=agregaVariables">
		Link al servlet controlador para agregar las variables
	</a>
	
	<br><br>
	<a href="${pageContext.request.contextPath}/ServletControlador?accion=listarVariables">
		Link al servlet controlador para listar variables
	</a>
</body>
</html>