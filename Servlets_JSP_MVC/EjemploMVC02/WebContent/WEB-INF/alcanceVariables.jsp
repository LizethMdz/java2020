<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Despliegue del Alcance variables</title>
</head>
<body>
	<h1>Despliegue del Alcance variables</h1>

	<br><br>
	Variable en alcance request: 
	<br><br>
	Base ${recRequest.base}
	<br><br>
	Altura ${recRequest.altura}
	<br><br>
	Area ${recRequest.area}
	
	<br><br>
	Variable en alcance session: 
	<br><br>
	Base ${recSession.base}
	<br><br>
	Altura ${recSession.altura}
	<br><br>
	Area ${recSession.area}
	
	<br><br>
	Variable en alcance application: 
	<br><br>
	Base ${recApplication.base}
	<br><br>
	Altura ${recApplication.altura}
	<br><br>
	Area ${recApplication.area}
	
	
	<br><br>
	<a href="${pageContext.request.contextPath}/index.jsp">
		Regresar
	</a>
	
</body>
</html>