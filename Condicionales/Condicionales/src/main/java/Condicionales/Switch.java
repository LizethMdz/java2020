package Condicionales;

public class Switch {

    public static void main(String[] args){
        String grupoReprentativo = "Caballo de Troya";

        switch (grupoReprentativo) {
            case "UBUNTU":
                System.out.println("Bienvenido a UBUNTU");
                break;
            case "CD":
                System.out.println("Bienvenido a Centro de Desarrollo");
                break;
            case "PASCAL":
                System.out.println("Bienvenido a PASCAL");
                break;

            case "ESTIC":
                System.out.println("Bienvenido a ESTIC");
                break;
        
            default:
                System.out.println("Bienvenido a Cabayo de Troya!!!");
                break;
        }
    }

    
}