package Condicionales;

public class AnidacionIf {
    public static void main(String[] args){

        int edad = 18;

        if( edad == 18 ){
        
            if( edad > 18 || edad < 22 ){
                System.out.println("Puedes ir a fiestas");
            }
            if(edad < 19) {
                System.out.println(" Y Necesitas permiso de tus papás");
            }
            if(edad > 23) {
                System.out.println("Diviertete con medida");
            }
            
        }

    }
}