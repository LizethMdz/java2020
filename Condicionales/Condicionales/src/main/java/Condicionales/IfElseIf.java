package Condicionales;

public class IfElseIf {
    public static void main(String[] args){
        String grupo = "A";

        if( grupo == "D" ){
            System.out.println("Grupo D");
        }else if( grupo == "B"){    
            System.out.println("Grupo B");
        }else if( grupo == "C"){
            System.out.println("Grupo C");
        }else if( grupo == "A"){
            System.out.println("Grupo A");
        }else {
            System.out.print("No existe el grupo");
        }
    }
}