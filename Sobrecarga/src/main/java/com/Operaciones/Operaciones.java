package com.Operaciones;

public class Operaciones {
    public static int sumar(int a , int b){
        System.out.println("Metodo sumar(int a, int b)");
        return a + b;
    }

    public static double sumar(double a, double b){
        System.out.println("Metodo suma(double a, double b)");
        return a + b ;
    }

    public static double sumar(int a, double b){
        System.out.println("Metodo suma(int a, double b)");
        return a + b ;
    }

    public static double sumar(double a, int b){
        System.out.println("Metodo suma(double a, int b)");
        return a + b ;
    }
}