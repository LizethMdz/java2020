package com;

import com.Operaciones.Operaciones;

public class App{
    public static void main(String[] args){
        System.out.println(Operaciones.sumar(3, 5));
        System.out.println(Operaciones.sumar(3D, 5D));
        System.out.println(Operaciones.sumar(15, 5L));

        System.out.println(Operaciones.sumar(3F, 5));
        //byte(8) - short and char(16) - int(32) - long(64) - float(32) - double(64)
    
    }
}