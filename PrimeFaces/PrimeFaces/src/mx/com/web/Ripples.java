package mx.com.web;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
@Named("ripples")
@RequestScoped
public class Ripples {
	
	public List<String> completeArea(String cadena){
		List<String> results = new ArrayList<>();
		if(cadena.equalsIgnoreCase("Days")) {
			results.add("Day Monday");
			results.add("Day Tuesday");
			results.add("Day Wednesday");
			results.add("Day Thursday");
			results.add("Day Friday");
			results.add("Day Saturday");
			results.add("Day Sunday");
			results.add("Unknown");
		}else {
			for(int i = 0; i < 10; i++) {
				results.add(cadena + i);
			}
			
		}
		
		return results;
	}
}
